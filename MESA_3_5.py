#!/usr/bin/env python
#coding: utf8

import ast
from RingModInstrument import RingModInstrument
from GUI import *

# To see which driver is which
#inputDriverList, inputDriverIndexes = pa_get_input_devices()
#defaultInputDriver = inputDriverList[inputDriverIndexes.index(pa_get_default_input())]
#outputDriverList, outputDriverIndexes = pa_get_output_devices()
#defaultOutputDriver = outputDriverList[outputDriverIndexes.index(pa_get_default_output())]
#print inputDriverList
#print inputDriverIndexes
#print outputDriverList
#print outputDriverIndexes

#TODO: Ajouter la possiblité de choisir quels channels de l'interface on veut utiliser (et updater le Save et load en conséquence)
ins=outs=2
app = wx.App(False)

dlg = ConfigurationDialog(None)
dlg.CenterOnScreen()
val = dlg.ShowModal()

if val == wx.ID_OK:
    if dlg.radioNew.GetValue() == 1:
        #Ring modulator
        inDevice, outDevice = dlg.getDevicesIndexesFromString()
        ins = int(dlg.noInChnls.GetValue())
        outs = int(dlg.noOutChnls.GetValue())
        ringMod = RingModInstrument(inDevice, outDevice, ins, outs)

        # GUI
        mainFrame = MainFrame(None, noInputs=ins, noOutputs=outs)
    elif dlg.radioOpen.GetValue() == 1:
        # Retrieve values from the save file
        try:
            fp = file(dlg.path.GetValue(), 'r') 
            dict = ast.literal_eval(fp.read())
            fp.close()
            # Ring modulator
            inDevice, outDevice, maxInChnls, maxOutChnls = dlg.getDevicesIndexesAndMaxChannels(dict['inputDriver'], dict['outputDriver'], dict['maxInChnls'], dict['maxOutChnls'])
            ringMod = RingModInstrument(inDevice, outDevice, dict['nbOfIns'], dict['nbOfOuts'], maxInChnls, maxOutChnls, dict['carrierChnlNo'], dict['modulatorChnlNo'], dict['sessionFile'], dict['saveDir'], dict)

            # GUI
            mainFrame = MainFrame(None, noInputs=dict['nbOfIns'], noOutputs=dict['nbOfOuts'], midiCtls=dict['midiCtls'])
        except IOError:
            raise SystemExit
    dlg.Destroy()
    mainFrame.Show()
    app.MainLoop()