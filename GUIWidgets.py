#!/usr/bin/env python
# encoding: utf-8
import wx, math, sys
#from _wxwidgets import VuMeter
from pyo import *


from pyolib._wxwidgets import VuMeter


####################################################
##### Constants that I didn't want to strip! #######
####################################################
PLATFORM = sys.platform

BACKGROUND_COLOUR = (232,232,232) #'#DDDDDD'
CONTROLSLIDER_KNOB_COLOUR = '#DDDDDD'
CONTROLSLIDER_DISABLE_KNOB_COLOUR = '#ABABAB'
CONTROLSLIDER_BACK_COLOUR = '#BBB9BC'
CONTROLSLIDER_BACK_COLOUR_INTERP = '#CCCACD'
CONTROLSLIDER_DISABLE_BACK_COLOUR = '#99A7AA'
CONTROLSLIDER_SELECTED_COLOUR = '#EAEAEA'
CONTROLSLIDER_TEXT_COLOUR = '#000000'
FONT_FACE = 'Trebuchet MS'
#Trebuchet MS
if sys.platform in ['linux2', 'win32']:
    CONTROLSLIDER_FONT = 7
else:
    CONTROLSLIDER_FONT = 10

# TODO: ne pas assigner les args aux vumetres si ceux-ci n'existent plus

####################################################
############### Mandatory Utilities ################
####################################################
def interpFloat(t, v1, v2):
    "interpolator for a single value; interprets t in [0-1] between v1 and v2"
#    print 'interp.: '+str(v1), str(v2)
    return float((v2-v1)*t + v1)

def tFromValue(value, v1, v2):
    "returns a t (in range 0-1) given a value in the range v1 to v2"
#    print v1, v2, value, float(value-v1)/(v2-v1)
    return float(value-v1)/(v2-v1)

# Added this here because it doesn't seem to see it in the _wxwidgets file...
def toLog(t, v1, v2):
    return math.log10(t/v1) / math.log10(v2/v1)
    
# Same here...
def toExp(t, v1, v2):
    return math.pow(10, t * (math.log10(v2) - math.log10(v1)) + math.log10(v1))

def clamp(v, minv, maxv):
    "clamps a value within a range"
    if v<minv: v=minv
    if v> maxv: v=maxv
    return v

# Main class for the meter/slider
# Added the backColour and meterWidth args. 
# Adapted two be able to display stereo signals
#class MeterControlSlider(wx.Panel):
#    def __init__(self, parent, minvalue, maxvalue, init=None, pos=(0,0),
#                 size=(28,150), outFunction=None, outLinValue=False,
#                 backColour=None, meterWidth=8, numSliders=2, orient=wx.HORIZONTAL):
#        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=pos, size=size,
#                            style=wx.NO_BORDER | wx.WANTS_CHARS | wx.EXPAND)
#        self.parent = parent
#        if backColour:
#            self.backgroundColour = backColour
#        else:
#            self.backgroundColour = BACKGROUND_COLOUR
#        self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)
#        self.SetBackgroundColour(self.backgroundColour)
#        self.midiBackgroundColour = None
#        
#        # FL
#        if orient == wx.HORIZONTAL:
#            size = (size[0], numSliders * 5 + 1)
#        else:
#            size = (numSliders * 5 + 1, size[1])

#        # FL
#        self.old_nchnls = numSliders
#        self.numSliders = numSliders
#        self.orient = orient
#        
#        self.knobSize = 11
#        self.knobHalfSize = 5
#        self.sliderWidth = size[0] - 23
#        self.meterWidth = size[0]
#        self.meterHeight = 0
#        self.meterOffset = 0
#        self.meterOffset = 30

#        self.outLinValue = outLinValue
#        self.outFunction = outFunction
#        self.SetRange(minvalue, maxvalue)
#        self.selected = False
#        self.propagate = False
#        self.midictl = None
#        self.label = ''
#        self.new = ''
#        if init != None:
#            self.SetValue(init, False)
#        else:
#            self.SetValue(minvalue, False)
#        self.clampPos()
#        
#        # FL
#        self.amplitude = self.last_amplitude = [0.0] * self.numSliders
#        self.amplitude = self.last_amplitude = 0.0 * self.numSliders

#        self.greybrush = wx.Brush("#444444")
#        self.selectbrush = wx.Brush("#CCCCCC")
#        self.selectpen = wx.Pen("#CCCCCC")
#        self.textrect = wx.Rect(0, size[1]-29, size[0], 16)
#        self.hlrect = wx.Rect(0, size[1]-28, size[0], 13)
#        self.midirect = wx.Rect(0, size[1]-16, size[0], 16)
        
#        self.needBackground = False
#        self.needBackground = True
#        self.backBitmap = None
#        self.createKnobBitmap()
#        self.createMeterBitmap()

#        self.Bind(wx.EVT_LEFT_DOWN, self.MouseDown)
#        self.Bind(wx.EVT_LEFT_UP, self.MouseUp)
#        self.Bind(wx.EVT_LEFT_DCLICK, self.DoubleClick)
#        self.Bind(wx.EVT_MOTION, self.MouseMotion)
#        self.Bind(wx.EVT_PAINT, self.OnPaint)
#        self.Bind(wx.EVT_SIZE, self.OnResize)
#        self.Bind(wx.EVT_KEY_DOWN, self.keyDown)
#        self.Bind(wx.EVT_KILL_FOCUS, self.LooseFocus)
#        self.Bind(wx.EVT_CLOSE, self.OnClose)

#        if sys.platform in ['win32', 'linux2']:
#            self.font = wx.Font(6, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL,
#                                wx.FONTWEIGHT_BOLD, face="Monospace")
#        else:
#            self.font = wx.Font(9, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL,
#                                wx.FONTWEIGHT_NORMAL, face="Monospace")
#                                
#    def setNumSliders(self, numSliders):
#        w, h = self.meterWidth, self.GetSize()[1]
#        w, h = self.meterWidth, self.GetSize()[1] - self.meterOffset
#        oldChnls = self.old_nchnls
#        self.numSliders = numSliders
#        self.amplitude = [0] * self.numSliders
#        gap = (self.numSliders - oldChnls) * 5
#        parentSize = self.parent.GetSize()
#        if self.orient == wx.HORIZONTAL:
#            self.SetSize((w, self.numSliders * 5 + 1))
#            self.SetMinSize((w, 5*self.numSliders+1))
#            self.parent.SetSize((parentSize[0], parentSize[1]+gap))
#            self.parent.SetMinSize((parentSize[0], parentSize[1]+gap))
#        else:
#            self.SetSize((self.numSliders * 5 + 1, h))
#            self.SetMinSize((5*self.numSliders+1, h))
#            self.parent.SetSize((parentSize[0]+gap, parentSize[1]))
#            self.parent.SetMinSize((parentSize[0]+gap, parentSize[1]))
#        wx.CallAfter(self.Refresh)
#        wx.CallAfter(self.parent.Layout)
#        wx.CallAfter(self.parent.Refresh)

#    def createKnobBitmap(self):
#        b = wx.EmptyBitmap(10, self.knobSize)
#        dc = wx.MemoryDC(b)
#        gc = wx.GraphicsContext_Create(dc)
#        dc.SetBackground(wx.Brush(self.backgroundColour))
#        dc.Clear()
#        gc.SetPen(wx.Pen("#444444"))
#        gc.SetBrush(self.greybrush)
#        p1 = (1, 1)
#        p2 = (1, self.knobSize-1)
#        p3 = (9, self.knobHalfSize)
#        gc.DrawLines([p1,p2,p3])
#        dc.SelectObject(wx.NullBitmap)
#        self.knobBitmap = b

#    # FL: Everything with the background drawing ('b')
#    def createMeterBitmap(self):
#        w, h = self.meterWidth, self.GetSize()[1]
#        w, h = self.meterWidth, self.GetSize()[1] - self.meterOffset
#        b = wx.EmptyBitmap(w,h)
#        f = wx.EmptyBitmap(w,h)
#        dcb = wx.MemoryDC(b)
#        dcf = wx.MemoryDC(f)
#        dcb.SetPen(wx.Pen("#000000", width=1))
#        dcf.SetPen(wx.Pen("#000000", width=1))
#        steps = int(h / 10.0 + 0.5)
#        # FL
#        if self.orient == wx.HORIZONTAL:
#            height = 6
#            steps = int(w / 10.0 + 0.5)
#        else:
#            width = 6
#            steps = int(h / 10.0 + 0.5)
#        bounds = int(steps / 6.0)
#        for i in range(steps):
#            if i == (steps - 1):
#                dcb.SetBrush(wx.Brush("#770000"))
#                dcf.SetBrush(wx.Brush("#FF0000"))
#            elif i >= (steps - bounds):
#                dcb.SetBrush(wx.Brush("#440000"))
#                dcf.SetBrush(wx.Brush("#CC0000"))
#            elif i >= (steps - (bounds*2)):
#                dcb.SetBrush(wx.Brush("#444400"))
#                dcf.SetBrush(wx.Brush("#CCCC00"))
#            else:
#                dcb.SetBrush(wx.Brush("#004400"))
#                dcf.SetBrush(wx.Brush("#00CC00"))
#            ii = steps - 1 - i
#            dcb.DrawRectangle(0, ii*10, width, 11)
#            dcf.DrawRectangle(0, ii*10, width, 11)
#            dcf.DrawRectangle(0, ii*10, w, 11)
#        if self.orient == wx.HORIZONTAL:
#            dcb.DrawLine(w-1, 0, w-1, height)
#            dcf.DrawLine(w-1, 0, w-1, height)
#        else:
#            dcb.DrawLine(0, 0, width, 0)
#            dcf.DrawLine(0, 0, width, 0)
#        dcf.DrawLine(0, 0, w, 0)
#        dcb.SelectObject(wx.NullBitmap)
#        dcf.SelectObject(wx.NullBitmap)
#        self.backBitmap = b
#        self.bitmap = f

#    def setRms(self, *args):
#        if args[0] < 0:
#            return
#        if not args:
#            # FL
#            self.amplitude = [0 for i in range(self.numSliders)]
#            self.amplitude = 0.0
#        else:
#            #FL (Dunno if the except works though... especially the wx.PyDeadObjectError)
#            try:
#                self.amplitude = args
#            except (ReferenceError, wx.PyDeadObjectError):
#                pass
#            self.amplitude = args[0]
#        try:
#            if self.amplitude != self.last_amplitude:
#                self.last_amplitude = self.amplitude
#                # FL
#    #            width = 6
#                for i in range(self.numSliders):
#    #                y = i * (width - 1)
#    #                h = self.GetSize()[1] - self.meterOffset
#    #                db = math.log10(self.amplitude[i]+0.00001) * 0.2 + 1.
#    #                self.meterHeight = int(db * h)
#    #                self.needBackground = False
#                    h = self.GetSize()[1]
#                    h = self.GetSize()[1] - self.meterOffset
#                    try:
#                        db = math.log10(self.amplitude[i]+0.00001) * 0.2 + 1.
#                    except:
#                        db = math.log10(self.amplitude[i-1]+0.00001) * 0.2 + 1.
#                    self.meterHeight = int(db * h)
#                    self.needBackground = False
#                wx.CallAfter(self.Refresh)
#        except (ReferenceError, wx.PyDeadObjectError):
#            pass
#        wx.CallAfter(self.Refresh)

#    def setMidiCtl(self, x, propagate=True):
#        self.propagate = propagate
#        self.midictl = x
#        wx.CallAfter(self.Refresh)

#    def getMidiCtl(self):
#        return self.midictl

#    def getMinValue(self):
#        return self.minvalue

#    def getMaxValue(self):
#        return self.maxvalue

#    def setSliderHeight(self, height):
#        self.sliderHeight = height
#        wx.CallAfter(self.Refresh)

#    def setSliderWidth(self, width):
#        self.sliderWidth = width
#        wx.CallAfter(self.Refresh)

#    def SetRange(self, minvalue, maxvalue):
#        self.minvalue = minvalue
#        self.maxvalue = maxvalue

#    def getRange(self):
#        return [self.minvalue, self.maxvalue]

#    def scale(self):
#        h = self.GetSize()[1] - self.meterOffset
#        inter = tFromValue(h-self.pos, self.knobHalfSize, h-self.knobHalfSize)
#        return interpFloat(inter, self.minvalue, self.maxvalue)

#    # Changed default value of propagate to False, to avoid infinite loops situations with two sliders with each other being the linkedObject...
#    def SetValue(self, value, propagate=False):
#        # TODO: setting value is often ovveride by
#        # setRms calls and the label is not always updated
#        self.propagate = propagate
#        if self.HasCapture():
#            self.ReleaseMouse()
#        value = clamp(value, self.minvalue, self.maxvalue)
#        print 'Value: '+str(value)
#        t = tFromValue(value, self.minvalue, self.maxvalue)
#        print 'T: '+str(t)
#        self.value = interpFloat(t, self.minvalue, self.maxvalue)
#        print 'Final Value: '+str(self.value)
#        self.clampPos()
#        self.selected = False
#        self.valueChanged()
#        wx.CallAfter(self.Refresh)

#    def GetValue(self):
#        return self.value

#    def valueChanged(self):
#        if self.outFunction and self.propagate:
#            if self.outLinValue:
#                self.outFunction(pow(10, self.value * 0.05))
#            else:
#                print pow(10, self.value * 0.05)
#                self.outFunction(self.value)
#        self.propagate = True
#        self.labelChanged()

#    def labelChanged(self):
#        if self.selected and self.new:
#            val = self.new
#        else:
#            val = self.GetValue()
#            if abs(val) >= 100:
#                val = '%.0f' % val
#            elif abs(val) >= 10:
#                val = '%.1f' % val
#            elif abs(val) < 10:
#                val = '%.2f' % val
#        if self.GetValue() >= 0:
#            val = " " + val
#        self.label = val
#        wx.CallAfter(self.Refresh)

#    def LooseFocus(self, event):
#        self.selected = False
#        wx.CallAfter(self.Refresh)

#    def keyDown(self, event):
#        if self.selected:
#            char = ''
#            if event.GetKeyCode() in range(324, 334):
#                char = str(event.GetKeyCode() - 324)
#            elif event.GetKeyCode() == 390:
#                char = '-'
#            elif event.GetKeyCode() == 391:
#                char = '.'
#            elif event.GetKeyCode() == wx.WXK_BACK:
#                if self.new != '':
#                    self.new = self.new[0:-1]
#                    self.labelChanged()
#            elif event.GetKeyCode() < 256:
#                char = chr(event.GetKeyCode())
#            if char in ['0','1','2','3','4','5','6','7','8','9','.','-']:
#                self.new += char
#                self.labelChanged()
#            elif event.GetKeyCode() in [wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER]:
#                self.SetValue(eval(self.new))
#                self.new = ''
#                self.selected = False
#            wx.CallAfter(self.Refresh)
#        event.Skip()

#    def MouseDown(self, evt):
#        h = self.GetSize()[1] - self.meterOffset
#        posY = evt.GetPosition()[1]
#        if evt.ShiftDown():
#            self.DoubleClick(evt)
#            return
#        if posY < h:
#            self.pos = clamp(evt.GetPosition()[1], self.knobHalfSize, h)
#            self.value = self.scale()
#            self.CaptureMouse()
#            self.selected = False
#            self.valueChanged()
#            wx.CallAfter(self.Refresh)
#        evt.Skip()

#    def MouseUp(self, evt):
#        if self.HasCapture():
#            self.ReleaseMouse()

#    def DoubleClick(self, event):
#        w, h = self.GetSize()
#        pos = event.GetPosition()
#        if self.textrect.Contains(pos):
#            self.selected = True
#        wx.CallAfter(self.Refresh)
#        event.Skip()

#    def MouseMotion(self, evt):
#        posY = evt.GetPosition()[1]
#        h = self.GetSize()[1] - self.meterOffset
#        if evt.LeftIsDown() and evt.Dragging():
#            self.pos = clamp(posY, self.knobHalfSize, h-self.knobHalfSize)
#            self.value = self.scale()
#            print self.value
#            self.selected = False
#            self.valueChanged()
#            wx.CallAfter(self.Refresh)

#    def OnResize(self, evt):
#        w, h = self.GetSize()
#        self.createKnobBitmap()
#        self.createMeterBitmap()
#        self.textrect = wx.Rect(0, h-29, w, 16)
#        self.hlrect = wx.Rect(0, h-28, w, 13)
#        self.midirect = wx.Rect(0, h-16, w, 16)
#        self.clampPos()
#            self.drawBackground()
#        wx.CallAfter(self.Refresh)

#    def clampPos(self, value=None):
#        # FL: added the "value" param
#        if value == None:
#            value = self.value
#        h = self.GetSize()[1] - self.meterOffset
#        val = tFromValue(self.value, self.minvalue, self.maxvalue)
#            val = tFromValue(self.value, self.minvalue, self.maxvalue)

#        self.pos = (1.0 - val) * (h - self.knobSize) + self.knobHalfSize
#        self.pos = clamp(self.pos, self.knobHalfSize, h-self.knobHalfSize)

#    def setBackgroundColour(self, colour):
#        self.backgroundColour = colour
#        self.knobBitmap.SetBackground(colour)
#        self.SetBackgroundColour(self.backgroundColour)
#        wx.CallAfter(self.Refresh)

#    def setMidiBackgroundColour(self, colour):
#        self.midiBackgroundColour = colour
#        wx.CallAfter(self.Refresh)

#    def revertMidiBackgroundColour(self):
#        self.midiBackgroundColour = None
#        wx.CallAfter(self.Refresh)

#        def drawBackground(self):
#            w, h = self.GetSize()
#            bitmap = wx.EmptyBitmap(w, h-15)
#            dc = wx.MemoryDC()
#            dc.SelectObject(bitmap)

#            dc.SetBackground(wx.Brush(self.backgroundColour))
#            dc.Clear()

#            if self.selected:
#                dc.SetPen(self.selectpen)
#                dc.SetBrush(self.selectbrush)
#                dc.DrawRectangleRect(self.hlrect)

#            dc.SetFont(self.font)
#            dc.SetTextForeground('#000000')
#            #Alignement du label en dessous
#            dc.DrawLabel(self.label, self.textrect,
#                         wx.ALIGN_CENTER|wx.ALIGN_CENTER_VERTICAL)

#            if self.midiBackgroundColour:
#                dc.SetPen(wx.Pen(self.midiBackgroundColour))
#                dc.SetBrush(wx.Brush(self.midiBackgroundColour))
#                dc.DrawRectangleRect(self.midirect)

#            if self.midictl is None:
#                midilabel = "M: ?"
#            else:
#                midilabel = "M: %d" % self.midictl
#            dc.DrawLabel(midilabel, self.midirect, wx.ALIGN_CENTER)

#            dc.SelectObject(wx.NullBitmap)
#            self.backBitmap = bitmap
#    #        
#        def OnPaint(self, evt):
#        w, h = self.GetSize()
#        dc = wx.AutoBufferedPaintDC(self)

#            if self.needBackground or self.backBitmap is None:
#                self.drawBackground()
#            dc.DrawBitmap(self.backBitmap, 11, 0)

#        # Draw meter
#        dc.SetBrush(self.greybrush)
#        dc.Clear()
#        #FL-START
#        dc.DrawRectangle(11, 0, self.meterWidth, h-+1)
#            dc.DrawRectangle(11, 0, self.meterWidth, h-self.meterOffset+1)
#        width = 6
#        for i in range(self.numSliders):
#            y = i * (width - 1)
#            h = self.GetSize()[1]
#                h = self.GetSize()[1] - self.meterOffset
#            try:
#                db = math.log10(self.amplitude[i]+0.00001) * 0.2 + 1.
#            except:
#                db = math.log10(self.amplitude[i-1]+0.00001) * 0.2 + 1.
#            self.meterHeight = int(db * h)
#            dc.DrawBitmap(self.backBitmap, y+11, 0)
#            self.needBackground = True
#        # FL-END    
#        
#            if self.meterHeight > 0:
#                dc.SetClippingRegion(y+11, h-self.meterHeight,
#                                     width, self.meterHeight)
#    #                                 dc.SetClippingRegion(11, h-self.meterHeight,
#    #                                 self.meterWidth, self.meterHeight)
#                dc.DrawBitmap(self.bitmap, y+11, 0)
#                dc.DestroyClippingRegion()

#        dc.DrawBitmap(self.knobBitmap, 0, self.pos-self.knobHalfSize)

#        self.needBackground = True

#        evt.Skip()
#        
#    def OnClose(self, evt):
#        self.Destroy()

# Child class for the meter/slider
#class QLiveControlSlider(MeterControlSlider):
#    def __init__(self, parent, minvalue, maxvalue, init=None, pos=(0,0), 
#                 size=(200,16), log=False, outFunction=None, integer=False, 
#                 powoftwo=False, backColour=None, orient=wx.HORIZONTAL, 
#                 linkedObject=None, meterWidth=8, numSliders=2, name="Slider"):
#        MeterControlSlider.__init__(self, parent, minvalue, maxvalue, 
#                                    init, pos, size, self.localOutFunction, 
#                                    False, backColour, meterWidth, numSliders, orient)
#        self.channelobject = None
#        self.midiscanning = False
#        self.linkedObject = None
#        self.name = name
#        self.externalOutFunction = outFunction
#        self.Bind(wx.EVT_RIGHT_DOWN, self.MouseRightDown)

#    def setOutFunction(self, func):
#        self.externalOutFunction = func

#    def localOutFunction(self, value):
#        if self.linkedObject:
#            self.linkedObject.SetValue(value)
#        value = pow(10.0, value * 0.05)
#        if self.externalOutFunction is not None:
#            self.externalOutFunction(value)

#    def setLinkedObject(self, obj):
#        self.linkedObject = obj

#    def setChannelObject(self, obj):
#        self.channelobject = obj

#    def MouseRightDown(self, evt):
#        if evt.ShiftDown():
#            print "Remove MIDI binding"
#            #QLiveLib.getVar("MidiServer").unbind("ctls", self.midictl, self.midi)
#            self.setMidiCtl(None)
#            return
#        if not self.midiscanning:
#            self.midiscanning = True
#            print "Start MIDI scanning"
#            #QLiveLib.getVar("MidiServer").ctlscan(self.getMidiScan)
#            self.setMidiBackgroundColour(MIDILEARN_COLOUR)
#        else:
#            self.midiscanning = False
#            print "Stop MIDI scanning"
#            #QLiveLib.getVar("MidiServer").ctlscan(None)
#            self.revertMidiBackgroundColour()
#            
#    def getMidiScan(self, ctlnum, midichnl):
#        self.assignMidiCtl(ctlnum)
#        self.revertMidiBackgroundColour()

#    #FL
#    def GetName(self):
#        return self.name
#        
#    def assignMidiCtl(self, ctlnum):
#        self.setMidiCtl(ctlnum)
#        print "In MIDI scan: receive ctl number %d." % ctlnum 
#        #QLiveLib.getVar("MidiServer").bind("ctls", ctlnum, self.midi)

#    def midi(self, value):
#        self.SetValue(rescale(value, 0, 127, -90, 18))

# Added labelStyle arg. Either 'Bottom' (Normal behavior) or 'Side', which displays the label on the side of the knob. This was added for the Gain knob
class QLiveControlKnob(wx.Panel):
    def __init__(self, parent, minvalue, maxvalue, init=None, pos=(0,0),
                 size=(50,85), log=False, outFunction=None, integer=False,
                 backColour=None, label='', showAutomation=False,
                 editFunction=None, outOnShiftFunction=None, knobRadius=45, drawBG=True, labelStyle='Bottom', name="Knob"):
        wx.Panel.__init__(self, parent=parent, id=wx.ID_ANY, pos=pos,
                          size=size, style=wx.NO_BORDER|wx.WANTS_CHARS)
                          
        self.parent = parent
        self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)
        self.SetBackgroundColour(BACKGROUND_COLOUR)
        self.outFunction = outFunction
        self.editFunction = editFunction
        self.showAutomation = showAutomation
        self.labelStyle = labelStyle
        self.knobRadius = knobRadius
        self.name = name
        if self.showAutomation:
            self.drawBottomPart = True
        else:
            # Taille de la boite en background
            if self.labelStyle=='Side':
                self.SetSize((60, 100))
            else:
                self.SetSize(size)
            self.drawBottomPart = False
        self.SetMinSize(self.GetSize())
        self.outOnShiftFunction = outOnShiftFunction
        self.integer = integer
        self.log = log
        self.label = label
        self.SetRange(minvalue, maxvalue)
        self.borderWidth = 1
        self.selected = False
        self._enable = True
        self.new = ''
        self.floatPrecision = '%.3f'
        self.mode = 0
        self.midiLearn = False
        self.midictl = None
        self.autoPlay = False
        self.showEdit = False
        self.drawBG = drawBG
        self.colours = {0: "#000000", 1: "#FF0000", 2: "#00FF00"}
        if backColour: self.backColour = backColour
        else: self.backColour = CONTROLSLIDER_BACK_COLOUR
        if init != None:
            self.SetValue(init)
            self.init = init
        else:
            self.SetValue(minvalue)
            self.init = minvalue
        self.Bind(wx.EVT_LEFT_DOWN, self.MouseDown)
        self.Bind(wx.EVT_LEFT_UP, self.MouseUp)
        self.Bind(wx.EVT_LEFT_DCLICK, self.DoubleClick)
        self.Bind(wx.EVT_MOTION, self.MouseMotion)
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_KEY_DOWN, self.keyDown)
        self.Bind(wx.EVT_KILL_FOCUS, self.LooseFocus)

#        if PLATFORM == "win32":
#            self.dcref = wx.BufferedPaintDC
#        else:
#            self.dcref = wx.PaintDC

    def setFloatPrecision(self, x):
        self.floatPrecision = '%.' + '%df' % x
        wx.CallAfter(self.Refresh)

    def getMinValue(self):
        return self.minvalue

    def getMaxValue(self):
        return self.maxvalue

    def setEnable(self, enable):
        self._enable = enable
        wx.CallAfter(self.Refresh)

    def getInit(self):
        return self.init

    def getLabel(self):
        return self.label

    def getLog(self):
        return self.log

    def SetRange(self, minvalue, maxvalue):
        self.minvalue = minvalue
        self.maxvalue = maxvalue

    def getRange(self):
        return [self.minvalue, self.maxvalue]

    def SetValue(self, value, propagate=False):
        value = clamp(value, self.minvalue, self.maxvalue)
        if self.log:
            t = toLog(value, self.minvalue, self.maxvalue)
            self.value = interpFloat(t, self.minvalue, self.maxvalue)
        else:
            t = tFromValue(value, self.minvalue, self.maxvalue)
            self.value = interpFloat(t, self.minvalue, self.maxvalue)
        if self.integer:
            self.value = int(self.value)
        self.selected = False
        if propagate:
            if self.outFunction:
                self.outFunction(self.GetValue())
        wx.CallAfter(self.Refresh)

    def setAutoPlay(self, state):
        self.autoPlay = state
        wx.CallAfter(self.Refresh)

    def setShowEdit(self, state):
        self.showEdit = not self.showEdit
        wx.CallAfter(self.Refresh)

    def GetValue(self):
        if self.log:
            t = tFromValue(self.value, self.minvalue, self.maxvalue)
            val = toExp(t, self.minvalue, self.maxvalue)
        else:
            val = self.value
        if self.integer:
            val = int(val)
        return val

    def LooseFocus(self, event):
        self.selected = False
        wx.CallAfter(self.Refresh)

    def keyDown(self, event):
        if self.selected:
            char = ''
            if event.GetKeyCode() in range(324, 334):
                char = str(event.GetKeyCode() - 324)
            elif event.GetKeyCode() == 390:
                char = '-'
            elif event.GetKeyCode() == 391:
                char = '.'
            elif event.GetKeyCode() == wx.WXK_BACK:
                if self.new != '':
                    self.new = self.new[0:-1]
            elif event.GetKeyCode() < 256:
                char = chr(event.GetKeyCode())
            if char in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
                self.new += char
            elif char == '.' and not '.' in self.new:
                self.new += char
            elif char == '-' and len(self.new) == 0:
                self.new += char
            elif event.GetKeyCode() in [wx.WXK_RETURN, wx.WXK_NUMPAD_ENTER]:
                if self.new != '':
                    self.SetValue(eval(self.new))
                    # Send value
                    if self.outFunction:
                        self.outFunction(self.GetValue())
                    self.new = ''
                self.selected = False
        wx.CallAfter(self.Refresh)

    def setMidiCtl(self, ctl):
        self.midictl = ctl
        wx.CallAfter(self.Refresh)

    def setMidiLearn(self, state):
        self.midiLearn = state
        wx.CallAfter(self.Refresh)

    def MouseDown(self, evt):
        w, h = self.GetSize()
        if self._enable:
            # To ensure good behavior with the smaller gain knob
            if self.knobRadius < 40:
                rec = wx.Rect(0, 0, 30, 30)
            else:
                rec = wx.Rect(5, 13, 45, 45)
            pos = evt.GetPosition()
            if rec.Contains(pos):
                self.clickPos = wx.GetMousePosition()
                self.oldValue = self.value
                self.CaptureMouse()
                self.selected = False
            rec = wx.Rect(w-15, 69, 15, 15)
            if rec.Contains(pos):
                self.showEdit = not self.showEdit
                self.editFunction(self.showEdit)
            wx.CallAfter(self.Refresh)
        evt.Skip()

    def MouseUp(self, evt):
        if self.HasCapture():
            self.ReleaseMouse()

    def DoubleClick(self, event):
        if self._enable:
            w, h = self.GetSize()
            pos = event.GetPosition()
            reclab = wx.Rect(3, 60, w-3, 10)
            recpt = wx.Rect(self.knobPointPos[0]-3,
                            self.knobPointPos[1]-3, 9, 9)
            if reclab.Contains(pos):
                self.selected = True
            elif recpt.Contains(pos):
                self.mode = (self.mode+1) % 3
            wx.CallAfter(self.Refresh)
        event.Skip()

    def MouseMotion(self, evt):
        if self._enable:
            if evt.Dragging() and evt.LeftIsDown() and self.HasCapture():
                pos = wx.GetMousePosition()
                offY = self.clickPos[1] - pos[1]
                offX = pos[0] - self.clickPos[0]
                off = offY + offX
                off *= 0.005 * (self.maxvalue - self.minvalue)
                self.value = clamp(self.oldValue + off, self.minvalue,
                                   self.maxvalue)
                # Send value
                if self.outFunction:
                    self.outFunction(self.GetValue())
                if evt.ShiftDown() and self.outOnShiftFunction:
                    self.outOnShiftFunction(self.GetValue())
                self.selected = False
                wx.CallAfter(self.Refresh)

    def setbackColour(self, colour):
        self.backColour = colour
        wx.CallAfter(self.Refresh)
    
    #FL
    def GetName(self):
        return self.name

    def OnPaint(self, evt):
        # TODO: Drawing of the controlKnob must really be optimized
        w,h = self.GetSize()
        # PLEASE WORK
        dc = wx.PaintDC(self)
#        dc = self.dcref(self)
        gc = wx.GraphicsContext.Create(dc)

        if self._enable:
            backColour = self.backColour
            knobColour = CONTROLSLIDER_KNOB_COLOUR
        else:
            backColour = CONTROLSLIDER_DISABLE_BACK_COLOUR
            knobColour = CONTROLSLIDER_DISABLE_KNOB_COLOUR

        dc.Clear()
        gc.SetBrush(wx.Brush(self.backColour, wx.SOLID))
        #bug
        # Draw background
        if self.drawBG == True:
            gc.SetPen(wx.Pen("#777777", width=self.borderWidth, style=wx.SOLID))
            gc.DrawRoundedRectangle(0, 0, w-1, h-1, 3)
        # TAILLE DU LABEL AU-DESSUS
        dc.SetFont(wx.Font(13, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL,
                           wx.FONTWEIGHT_NORMAL, face=FONT_FACE))
        dc.SetTextForeground(CONTROLSLIDER_TEXT_COLOUR)

        # Draw text label
        if self.labelStyle=='Side':
            reclab = wx.Rect(29, 2, w, 9)
            dc.DrawLabel(self.label, reclab, wx.ALIGN_LEFT)
        else:
            reclab = wx.Rect(0, 1, w, 9)
            dc.DrawLabel(self.label, reclab, wx.ALIGN_CENTER_HORIZONTAL)



        recval = wx.Rect(0, h-(h/4), w, 14)
#        recval = wx.Rect(0, 55, w, 14)

        if self.selected:
            gc.SetBrush(wx.Brush(CONTROLSLIDER_SELECTED_COLOUR, wx.SOLID))
            gc.SetPen(wx.Pen(CONTROLSLIDER_SELECTED_COLOUR, self.borderWidth))
            gc.DrawRoundedRectangle(2, h-(h/4), w-4, 12, 2)
#            gc.DrawRoundedRectangle(2, 55, w-4, 12, 2)


        r = math.sqrt(.1)
        #Position de la barre
        val = tFromValue(self.value, self.minvalue, self.maxvalue) * 0.86
        ph = val * math.pi * 2 - (3 * math.pi / 2.2)
        # TAILLE DU KNOB (GAIN)
        X = r * math.cos(ph)*self.knobRadius
        Y = r * math.sin(ph)*self.knobRadius
        gc.SetBrush(wx.Brush(knobColour, wx.SOLID))
        gc.SetPen(wx.Pen(self.colours[self.mode], width=2, style=wx.SOLID))
        self.knobPointPos = (X+25, Y+35)
        R = math.sqrt(X*X + Y*Y)
        if self.labelStyle=='Side':
            gc.DrawEllipse(15-R, 10-R, R*2, R*2)
            gc.StrokeLine(15, 10, X+15, Y+10)
        else:
            gc.DrawEllipse((w/2)-R, (h/2)-R, R*2, R*2)
            gc.StrokeLine((w/2), (h/2), X+(w/2), Y+(h/2))
#            gc.DrawEllipse(25-R, 35-R, R*2, R*2)
#            gc.StrokeLine(25, 35, X+25, Y+35)

        dc.SetFont(wx.Font(CONTROLSLIDER_FONT, wx.FONTFAMILY_DEFAULT,
                           wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL,
                           face=FONT_FACE))
                           
# I do not want to display the value label, hence all the commented code

#        # Draw text value
        if self.selected and self.new:
            val = self.new
        else:
            if self.integer:
                val = '%d' % self.GetValue()
            else:
                val = self.floatPrecision % self.GetValue()
        if PLATFORM == 'linux2':
            width = len(val) * (dc.GetCharWidth() - 3)
        else:
            width = len(val) * dc.GetCharWidth()
        dc.SetTextForeground(CONTROLSLIDER_TEXT_COLOUR)
#        
        if self.labelStyle!='Side':
            dc.DrawLabel(val, recval, wx.ALIGN_CENTER)

#        if self.drawBottomPart:

#            dc.SetFont(wx.Font(CONTROLSLIDER_FONT, wx.FONTFAMILY_DEFAULT,
#                               wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL,
#                               face=FONT_FACE))
#            if self.midiLearn:
#                gc.SetPen(wx.Pen(MIDILEARN_COLOUR, 1))
#                gc.SetBrush(wx.Brush(MIDILEARN_COLOUR))
#                gc.DrawRoundedRectangle(3, 70, 43, 11, 2)
#            if self.midictl is not None:
#                dc.DrawLabel("M : %d" % self.midictl, wx.Rect(5, 72, 41, 11),
#                             wx.ALIGN_CENTER_VERTICAL)
#            else:
#                dc.DrawLabel("M : ?", wx.Rect(5, 72, 41, 11), wx.ALIGN_CENTER_VERTICAL)

#            if self.autoPlay:
#                gc.SetBrush(wx.Brush("#55DD55"))
#            else:
#                gc.SetBrush(wx.Brush("#333333", wx.TRANSPARENT))
#            if self.showEdit:
#                tri = [(w-14,72), (w-6,72), (w-10,80), (w-14,72)]
#            else:
#                tri = [(w-14,72), (w-14,80), (w-6,76), (w-14, 72)]
#            gc.SetPen(wx.Pen("#333333", 1.5))
#            gc.DrawLines(tri)

#        evt.Skip()

#############################################################
############### Little example of usage #####################
#############################################################
class MyFrame(wx.Frame):
    def __init__(self, parent, title, pos, size):
        wx.Frame.__init__(self, parent, -1, title, pos, size)

        self.server = Server().boot().start()
        self.stereo = Sine(freq=[200,250], mul=Randi(max=0.6, freq=[.5,.6])).out()
        
        self.panel = wx.Panel(self)
        self.panel.SetBackgroundColour(BACKGROUND_COLOUR)
        s = wx.BoxSizer(wx.HORIZONTAL)

        self.noise = Noise()
        self.noisePeak = PeakAmp(self.noise)

        # meter/slider
        self.meter = QLiveControlSlider(self.panel, -90, 18, 0, size=(28, 200),
                                        outFunction=self.onMeterSlider)
        # Assign the meter setter function to the PeakAmp object
        self.noisePeak.function = self.meter.setRms
#        self.meter.setChannelObject(self.noise)
        
        # Main vumeter
        self.meter2 = VuMeter(self.panel)
        # Assign it to the server
        self.server.setMeter(self.meter2)

        # Control knob
        self.knob = QLiveControlKnob(self.panel, 0, 1, outFunction=self.onKnob)
        
        s.Add(self.meter, 0, wx.ALL, 5)
        s.Add(self.meter2, 1, wx.ALL, 5)
        s.Add(self.knob, 0, wx.ALL, 5)
        self.panel.SetSizerAndFit(s)

    def onMeterSlider(self, value):
        """Slider/meter callback."""
        self.noise.mul = value
        print self.noise.mul

    def onKnob(self, value):
        print "knob: %f" % value

if __name__ == "__main__":
    app = wx.App(False)
    mainFrame = MyFrame(None, title='Simple App', pos=(100,100), size=(500,300))
    mainFrame.Show()
    app.MainLoop()
