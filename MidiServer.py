#!/usr/bin/env python
# encoding: utf-8
from pyo import *

# Code adapted from QLIVE by Olivier Bélanger
class MidiServer:
    def __init__(self):
        self.ctlscan_callback = None
        self.noteonscan_callback = None
        self.bindings = {"ctls": {}, "noteon": {}}
        self.listen = MidiListener(self._midirecv, 20)
        self.listen.start()

    def _midirecv(self, status, data1, data2):
        #print status, data1, data2
        if status & 0xF0 == 0x90 and data2 != 0: # noteon
            midichnl = status - 0x90 + 1
            if self.noteonscan_callback is not None:
                self.noteonscan_callback(data1, midichnl)
                self.noteonscan_callback = None
            elif data1 in self.bindings["noteon"]:
                for callback in self.bindings["noteon"][data1]:
                    callback(data1, data2)
        if status & 0xF0 == 0xB0: # control change
            midichnl = status - 0xB0 + 1
            if self.ctlscan_callback is not None:
                self.ctlscan_callback(data1, midichnl)
                self.ctlscan_callback = None
            if data1 in self.bindings["ctls"]:
                for callback in self.bindings["ctls"][data1]:
                    callback(data2)

    def ctlscan(self, callback):
        self.ctlscan_callback = callback

    def noteonscan(self, callback):
        if self.noteonscan_callback is not None:
            self.noteonscan_callback(-1, -1)
        self.noteonscan_callback = callback
        
    def bind(self, group, x, callback):
        if x in self.bindings[group]:
            self.bindings[group][x].append(callback)
        else:
            self.bindings[group][x] = [callback]
        
    def unbind(self, group, x, callback):
        if x in self.bindings[group]:
            if callback in self.bindings[group][x]:
                self.bindings[group][x].remove(callback)
                if not self.bindings[group][x]:
                    del self.bindings[group][x]
