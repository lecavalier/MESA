#!/usr/bin/env python
#coding: utf8

import os, pprint, ast
from pyo import *
from InputLine import InputLine
from OutputLine import OutputLine
from RingMod import RingMod
from Generators import *
from MidiServer import MidiServer

# Pyo Server
s = Server(sr=48000, buffersize=128)

# Globals
ins = []        
outs = []
sineGen = noiseGen = None
sessionFile = ""
saveDir = ""
maxInChnls = 0
maxOutChnls = 0
# These two await implementation
audioInDevice = 2 
audioOutDevice = 2
midiServer = None
        
class RingModInstrument():
    # POSSIBLE FEATURE: Provide an array of channels to use at init. (min. 2)
    ringModRef = None
    carrierChnlNo = -1
    modulatorChnlNo = -1
    
    def __init__(self, inDevice, outDevice, nbOfIns=2, nbOfOuts=2, maxInputs=2, maxOutputs=2, carrierInit=-1, modulatorInit=-1, session="Unsaved session", dir=os.getcwd(), dictionnary=None, mul=1, add=0):
        global ins, outs, sineGen, noiseGen, sessionFile, saveDir, maxInChnls, maxOutChnls, audioInDevice, audioOutDevice, midiServer
        self._nbOfIns = nbOfIns
        self._nbOfOuts = nbOfOuts
        audioInDevice = inDevice
        audioOutDevice = outDevice
        maxInChnls = maxInputs
        maxOutChnls = maxOutputs
        
        RingModInstrument.carrierChnlNo = carrierInit
        RingModInstrument.modulatorChnlNo = modulatorInit
        sessionFile = session
        saveDir = dir
        dict = dictionnary
        if dict == None:
            maxInChnls = pa_get_input_max_channels(audioInDevice)
            maxOutChnls = pa_get_output_max_channels(audioOutDevice)
       
        # Server initialization
        s.setInputDevice(audioInDevice)
        s.setOutputDevice(audioOutDevice) 
        if self._nbOfIns > maxInChnls:
            s.setIchnls(maxInChnls)
        else:
            s.setIchnls(self._nbOfIns)
        if self._nbOfOuts > maxOutChnls:
            s.setNchnls(maxOutChnls)
        else:
            s.setNchnls(self._nbOfOuts)
        s.boot()
        time.sleep(1)
        
        s.deactivateMidi()       
        midiServer = MidiServer()
        
        # At least one input is required for the ring modulation to function (which is then the carrier and the mod. at the same time)
        if self._nbOfOuts < 1:
            self.nbOfOuts = 1
        
        for i in range(self._nbOfIns):
            if dict == None:
                ins.append(InputLine(i))
            else:
                key = 'In '+str(i)
                ins.append(InputLine((dict[key]['chnl'])%maxInChnls, dict[key]['freq'], dict[key]['q'], dict[key]['bypass'], 
                                dict[key]['shift'], dict[key]['slope'], dict[key]['mul']))
        
        if dict == None:
            sineGen = InputSine()
            noiseGen = InputNoise()
        else:
            sineGen = InputSine(dict['Sine']['freq'], dict['Sine']['mul'])
            noiseGen = InputNoise(dict['Noise']['freq'], dict['Noise']['q'], dict['Noise']['bypass'], dict['Noise']['mul'])

        if RingModInstrument.carrierChnlNo == -1:
            carrier = Sig(0)
        elif RingModInstrument.carrierChnlNo == -2:
            carrier = sineGen
        elif RingModInstrument.carrierChnlNo == -3:
            carrier = noiseGen
        else:
            carrier = ins[RingModInstrument.carrierChnlNo]
            
        if RingModInstrument.modulatorChnlNo == -1:
            mod = Sig(0)
        elif RingModInstrument.modulatorChnlNo == -2:
            mod = sineGen
        elif RingModInstrument.modulatorChnlNo == -3:
            mod = noiseGen
        else:
            mod = ins[RingModInstrument.modulatorChnlNo]
        
        if dict == None:
            self._ringMod = RingMod(carrier, mod)
        else:
            try:
                self._ringMod = RingMod(carrier, mod, dict['RingMod']['fadeCar'], dict['RingMod']['fadeMod'])
            except:
                self._ringMod = RingMod(carrier, mod)

        RingModInstrument.ringModRef = self._ringMod
        for i in range(self._nbOfOuts):
            if dict == None:
                outs.append(OutputLine(self._ringMod, env=ins[i%self._nbOfIns].follower).out([i]))
            else:
                key = 'Out '+str(i)
                outs.append(OutputLine(self._ringMod, dict[key]['drive'], dict[key]['thresh'], dict[key]['ratio'], dict[key]['risetime'], dict[key]['falltime'], 5, dict[key]['knee'], dict[key]['max'], env=ins[i%self._nbOfIns].follower, mul=dict[key]['mul']).out([i%maxOutChnls]))
        
        s.start()
        #TODO: Apply filtering to OutputLine objects
        
def changeInputChnl(inputNo, chnl):
    """Change the input channel on one InputLine object."""
    ins[inputNo].setChnl(chnl)

def setFilterFreq(inputNo, freq):
    """Change the freq. of the band-pass filter on one input."""
    ins[inputNo].setFreq(freq)

def setFilterQ(inputNo, q):
    """Change the Q of the band-pass filter on one input."""
    ins[inputNo].setQ(q)
    
def toggleFilterBypass(inputNo, state):
    """Bypass the band-pass filter on one input."""
    if state != True:
        x = False
    else:
        x = True
    ins[inputNo].setBypass(x)
    
def setSlope(inputNo, value):
    """Change the slope of the follower on one input."""
    ins[inputNo].setSlope(value)

def setShift(inputNo, value):
    """Change the shift of the freq. shifter on one input."""
    ins[inputNo].setShift(value)
    
def setInputGain(inputNo, value):
    """Change the the 'mul' value on one input."""
    ins[inputNo].setMul(value)
        
def toggleCarrier(inputNo, state):
    """Set or unset an input signal as the ring modulator carrier signal"""
    if state == 1:
        RingModInstrument.carrierChnlNo = inputNo
        RingModInstrument.ringModRef.setInputCar(ins[inputNo])
    else:
        RingModInstrument.carrierChnlNo = -1
        RingModInstrument.ringModRef.setInputCar(Sig(0))
        
def toggleModulator(inputNo, state):
    """Set or unset an input signal as the ring modulator modulating signal"""
    if state == 1:
        RingModInstrument.modulatorChnlNo = inputNo
        RingModInstrument.ringModRef.setInputMod(ins[inputNo])
    else:
        RingModInstrument.modulatorChnlNo = -1
        RingModInstrument.ringModRef.setInputMod(Sig(0))
        
def toggleModulatorSine(state):
    if state == 1:
        RingModInstrument.modulatorChnlNo = -2
        RingModInstrument.ringModRef.setInputMod(sineGen)
    else:
        RingModInstrument.modulatorChnlNo = -1
        RingModInstrument.ringModRef.setInputMod(Sig(0))
    
def toggleCarrierSine(state):
    if state == 1:
        RingModInstrument.carrierChnlNo = -2
        RingModInstrument.ringModRef.setInputCar(sineGen)
    else:
        RingModInstrument.carrierChnlNo = -1
        RingModInstrument.ringModRef.setInputCar(Sig(0))
    
def toggleModulatorNoise(state):
    if state == 1:
        RingModInstrument.modulatorChnlNo = -3
        RingModInstrument.ringModRef.setInputMod(noiseGen)
    else:
        RingModInstrument.modulatorChnlNo = -1
        RingModInstrument.ringModRef.setInputMod(Sig(0))
    
def toggleCarrierNoise(state):
    if state == 1:
        RingModInstrument.carrierChnlNo = -3
        RingModInstrument.ringModRef.setInputCar(noiseGen)
    else:
        RingModInstrument.carrierChnlNo = -1
        RingModInstrument.ringModRef.setInputCar(Sig(0))
    
def setOutputLevel(outputNo, x):
    """Set the output level of one output."""
    outs[outputNo].setMul(x)
    
def setSineFreq(value):
    sineGen.setFreq(value)

def setSineGain(value):
    sineGen.setMul(value)
    
def setNoiseFilterFreq(value):
    noiseGen.setFreq(value)
    
def setNoiseGain(value):
    noiseGen.setMul(value)
    
def setNoiseFilterQ(value):
    noiseGen.setQ(value)
    
def setDrive(outputNo, x):
    for o in outs:
        o.setDrive(x)
        
def setThresh(outputNo, x):
    for o in outs:
        o.setThresh(x)
    
def setRatio(outputNo, x):
    for o in outs:
        o.setRatio(x)
    
def setRisetime(outputNo, x):
    for o in outs:
        o.setRisetime(x)
    
def setFalltime(outputNo, x):
    for o in outs:
        o.setFalltime(x)
    
def setLookahead(outputNo, x):
    for o in outs:
        o.setLookahead(x)
    
def setKnee(outputNo, x):
    for o in outs:
        o.setKnee(x)
    
def setMax(outputNo, x):
    for o in outs:
        o.setMax(x)
        
def setCarrierFadeTime(value):
    RingModInstrument.ringModRef.setFadeCar(value)
    
def setModulatorFadeTime(value):
    RingModInstrument.ringModRef.setFadeMod(value)
    
def toggleBypassNoise(state):
    if state != True:
        x = False
    else:
        x = True
    noiseGen.setBypass(x)
    
def toggleMuteAll(state):
    for i in range(len(outs)):
        outs[i].mute(state)
    
def getFilterFreq(inputNo):
    return ins[inputNo].freq
    
def getFilterQ(inputNo):
    return ins[inputNo].q
    
def getShift(inputNo):
    return ins[inputNo].shift
    
def getSlope(inputNo):
    return ins[inputNo].slope
    
def getInputGain(inputNo):
    return ins[inputNo].mul
    
def getSineFreq():
    return sineGen.freq
    
def getSineGain():
    return sineGen.mul
    
def getNoiseFilterFreq():
    return noiseGen.freq
    
def getNoiseFilterQ():
    return noiseGen.q
    
def getNoiseGain():
    return noiseGen.mul

def getInputPeak(inputNo):
    return ins[inputNo].inputPeak
    
def getOutputPeak(outputNo):
    return outs[outputNo].outputPeak
    
def getSinePeak():
    return sineGen.inputPeak
    
def getNoisePeak():
    return noiseGen.inputPeak
    
def getOutputMul(outputNo):
    return outs[outputNo].mul
    
def setOutputMul(outputNo, value):
    outs[outputNo].setMul(value)
    
def isCarrier(inputNo):
    if RingModInstrument.carrierChnlNo == inputNo:
        return 1
    else:
        return 0
    
def isModulator(inputNo):
    if RingModInstrument.modulatorChnlNo == inputNo:
        return 1
    else:
        return 0
        
def isFilterBypassed(inputNo):
    return ins[inputNo].bypass     
    
def isNoiseFilterBypassed():
    return noiseGen.bypass
    
def getDrive(outputNo):
    return outs[outputNo].drive

def getThresh(outputNo):
    return outs[outputNo].thresh
    
def getRatio(outputNo):
    return outs[outputNo].ratio
    
def getRisetime(outputNo):
    return outs[outputNo].risetime
    
def getFalltime(outputNo):
    return outs[outputNo].falltime
    
def getLookahead(outputNo):
    return outs[outputNo].lookahead
    
def getKnee(outputNo):
    return outs[outputNo].knee
    
def getMax(outputNo):
    return outs[outputNo].max
    
def getModulatorFadeTime():
    return RingModInstrument.ringModRef.fadeCar
    
def getCarrierFadeTime():
    return RingModInstrument.ringModRef.fadeMod

def getSessionName():
    return sessionFile
    
def getSaveDir():
    return saveDir
    
def saveSession(filename, dir, ctls, isTemp=False):
    global sessionFile, saveDir
    if isTemp != True:
        sessionFile = filename
    saveDir = dir+'/'
    ext = '.txt'
    
    if filename[-4:] == ext:
        fullPath = dir+'/'+filename
    else:
        fullPath = dir+'/'+filename+ext
        
    inputDriverList, inputDriverIndexes = pa_get_input_devices()
    inputDriver = inputDriverList[inputDriverIndexes.index(audioInDevice)]
    outputDriverList, outputDriverIndexes = pa_get_output_devices()
    outputDriver = outputDriverList[outputDriverIndexes.index(audioOutDevice)]
    
    pp = pprint.PrettyPrinter(indent=4)
    dict = {'sessionFile': sessionFile, 'saveDir': saveDir, 'inputDriver': inputDriver, 'outputDriver': outputDriver, 
            'carrierChnlNo': RingModInstrument.carrierChnlNo, 'modulatorChnlNo': RingModInstrument.modulatorChnlNo,
            'maxInChnls': maxInChnls, 'maxOutChnls': maxOutChnls, 'nbOfIns': len(ins), 'nbOfOuts': len(outs), 'midiCtls': ctls}
    
    for i in range(len(ins)):
        dict['In '+str(i)] = ins[i].save()

    for o in range(len(outs)):
        dict['Out '+str(o)] = outs[o].save()
        
    dict['RingMod'] = RingModInstrument.ringModRef.save()
        
    dict['Sine'] = sineGen.save()
    dict['Noise'] = noiseGen.save()

    if os.path.isfile(fullPath) == True:
        os.remove(fullPath)
    
    fp = file(fullPath, 'w') 
    fp.write(pp.pformat(dict))
    fp.close()

def getMidiServer():
    return midiServer