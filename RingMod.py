#!/usr/bin/env python
#coding: utf8

from pyo import *

class RingMod(PyoObject):
    """
    Ring modulator
    
    Ring modulation is a signal-processing effect in electronics
    performed by multiplying two signals, where one is typically
    a sine-wave or another simple waveform.
    
    This specific ring modulator object can multiply a signal
    (carrier) by any other type of signal (modulator).
    
    This class was adapted from an example by Olivier Bélanger,
    which can be found at the following address:
    ajaxsoundstudio.com/pyodoc/tutorials/pyoobject1.html.

    :Parent: :py:class::'PyoObject'
    
    :Args:
        
        inputCar : PyoObject
            Carrier signal to process.
        inputMod : PyoObject
            Modulating signal.
            
    """
    def __init__(self, inputCar, inputMod, fadeCar=0.05, fadeMod=0.05, mul=1, add=0):
        PyoObject.__init__(self, mul, add)
        self._inputCar = inputCar
        self._inputMod = inputMod
        self._fadeCar = fadeCar
        self._fadeMod = fadeMod
        self._inputCar_fader = InputFader(inputCar)
        self._inputMod_fader = InputFader(inputMod)
        inCar_fader,inMod_fader,car_time,mod_time,mul,add,lmax = convertArgsToLists(self._inputCar_fader,self._inputMod_fader,self._fadeCar,self._fadeMod,mul,add)
        self._mod = Sig(inCar_fader, mul=inMod_fader)
        self._ring = Sig(self._mod, mul=mul*2, add=add)
        self._base_objs = self._ring.getBaseObjects()

    def setInputCar(self, x):
        """
        Replace the 'inputCar' attribute.
        
        :Args:
            
            x : PyoObject
                New signal to process.
                
        """
        self._inputCar = x
        self._inputCar_fader.setInput(x, self._fadeCar)
        
    def setInputMod(self, x):
        """
        Replace the 'inputMod' attribute.
        
        :Args:
            
            x : PyoObject
                New modulating signal.
                
        """
        self._inputMod = x
        self._inputMod_fader.setInput(x, self._fadeMod)
     
    def setFadeCar(self, x):
        """
        Replace the 'fadeCar' attribute.
        
        :Args:
            
            fadetime : float
                Crossfade time between old and new input.
                
        """
        self._fadeCar= x
    
    def setFadeMod(self, x):
        """
        Replace the 'fadeMod' attribute.
        
        :Args:
            
            fadetime : float
                Crossfade time between old and new input.
                
        """
        self._fadeMod= x
        
    def play(self, dur=0, delay=0):
        self._mod.play(dur, delay)
        return PyoObject.play(self, dur, delay)
        
    def stop(self):
        self._mod.stop()
        return PyoObject.stop(self)
        
    def out(self, chnl=0, inc=1, dur=0, delay=0):
        self._mod.play(dur, delay)
        return PyoObject.out(self, chnl, inc, dur, delay)
        
    def ctrl(self, map_list=None, title=None, wxnoserver=False):
        self._map_list = [SLMapMul(self._mul)]
        PyoObject.ctrl(self, map_list, title, wxnoserver)
        
    def save(self):
        dict = {'fadeCar': self._fadeCar, 'fadeMod': self._fadeMod}
        return dict
        
    @property # getter
    def inputCar(self):
        """PyoObject. Carrier signal to process."""
        return self._inputCar
    @inputCar.setter # setter
    def inputCar(self, x):
        self.setInputCar(x)
        
    @property
    def inputMod(self):
        """PyoObject. Modulating signal."""
        return self._inputMod
    @inputMod.setter
    def inputMod(self, x):
        self.setInputMod(x)

    @property # getter
    def fadeCar(self):
        """Float. Crossfade time for carrier signal."""
        return self._fadeCar
    @fadeCar.setter # setter
    def fadeCar(self, x):
        self.setFadeCar(x)
        
    @property
    def fadeMod(self):
        """Float. Crossfade time for modulator signal."""
        return self._fadeMod
    @fadeMod.setter
    def fadeMod(self, x):
        self.setFadeMod(x)