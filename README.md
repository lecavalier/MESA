# MESA
## Background
This software is part of a project by Jonathan Goldman to recreate and perform *MESA*, a work composed by
Gordon Mumma and originally performed with David Tudor in 1966. This software was developed in 2016, 50 years after the first performance.

*MESA* was originally the soundtrack to the *Places* performance, created by the
Merce Cunningham Dance Company. For the performance, Mumma placed 4 contact microphones
on a bandoneon played by Tudor. The output from the microphones was treated by 
electronics Mumma built himself, which basically applied ring-modulation between the
signals. The resulting signal was then sent to a quadraphonic speaker setup, with the amplitude of 
each of the output channels being modulated with a VCA by an enveloppe follower placed on each individual input line, 
monitoring the signal coming from the contact mics. It was performed live with David Tudor on the bandoneon and 
Gordon Mumma operating the electronics. *MESA* has also been, at some point, performed with three chromatic harmonicas instead
of the bandoneon. Altough the recorded versions of the performance vary greatly, it is approximately between 18 and 25 minutes in lenght.

This software was developed with an historical mindset, which is to say that it 
was intended as a faithful recreation of the electronics that Mumma used, with a 
few minor improvements and add-ons. 
It was developed according to notes, schematics and specifications written by Mumma 
himself. It was also developed according to the feedback given by
Ofer Pelz, who would operate the software during the performance.

This re-creation of MESA was performed once on February 27th 2017 at the Montréal/Nouvelles Musiques
festival, at the Agora Hydro-Quebec in Montreal. Ofer Pelz was on electronics and 
Jonathan Goldman on the bandoneon. Francis Lecavalier was at the mixing console 
to further spatialize the quadraphonic output of the software and to blend in the signal of two external 
air mics placed on the stage.

## Software Overview
The software is basically a ring-modulating matrix, allowing the user to modulates any live signal coming from a
microphone with any other signal. Aside from the 4 microphone outputs, one can also modulate
a signal with a sine wave and random noise. Various parameters are available to modulate the resulting sound, all of
which are MIDI-mappable. The output is in quad, but can also be stereo.

## Notes about development
This software was developed using Python 2.7 and makes extensive use of the [Pyo DSP library](http://ajaxsoundstudio.com/software/pyo/).
Major refactoring and porting on Python 3 are probably necessary. It is no longer actively developed.
