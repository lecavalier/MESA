#!/usr/bin/env python
#coding: utf8

import wx
from pyo import *
from GUIWidgets import *
from GUIControl import *
from wx.lib.wordwrap import wordwrap

VOL_SLIDER_MIN = -90.0
VOL_SLIDER_MAX = 18.0
MIDILEARN_COLOUR = '#c90202'

########### Static methods
def sSetMidiBackgroundColour(colour, o):
    pass
#    o.SetBackgroundColour(colour)
        
def sRevertMidiBackgroundColour(o):
    pass
#    o.SetBackgroundColour(None)
    
############ Classes
class ConfigurationDialog(wx.Dialog):
    def __init__(self, parent, ID=-1, title="Configuration", size=wx.DefaultSize, pos=wx.DefaultPosition, 
                style=wx.DEFAULT_DIALOG_STYLE):
        wx.Dialog.__init__(self, parent, ID, title, pos, size, style)
        
        self._parent = parent
        self.inputs, self.inputIndexes, self.defaultInput, self.outputs, self.outputIndexes, self.defaultOutput, self.midiInputs, self.midiInputIndexes, self.defaultMidiInput = self.getAvailableAudioMidiDrivers()
        self.inOutDevices = self.inOutIndexes = []
        self.inputChnls = self.outputChnls = []
        
        createPressed = False
        openPressed = False
        
        for i in range(len(self.outputs)):
            try:
                inputStr = self.inputs[i]
            except IndexError:
                inputStr = "---"
            outStr = self.outputs[i] 
                 
            self.inOutDevices.append(inputStr+"/"+outStr)
                
        sizer = wx.BoxSizer(wx.VERTICAL)
        boxOpen = wx.BoxSizer(wx.HORIZONTAL)

        self.radioNew = wx.RadioButton(self, -1, "Create new session", style=wx.RB_GROUP)
        sizer.Add(self.radioNew, 0, wx.ALIGN_CENTRE|wx.ALL, 5)

        boxAU = wx.BoxSizer(wx.HORIZONTAL)
        boxChnl = wx.BoxSizer(wx.HORIZONTAL)
        
        # Audio In/Out
        label = wx.StaticText(self, -1, "Audio In/Out Driver:")
        self.au = wx.ComboBox(self, -1, self.inOutDevices[0], choices=self.inOutDevices, style=wx.CB_DROPDOWN)
        
        boxAU.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        boxAU.Add(self.au, 1, wx.ALIGN_CENTRE|wx.ALL, 5)
        
        # Number of channels to use
        for i in range(self.getMaxChnls(self.inputIndexes[0])):
            self.inputChnls.append(str(i+1))
        for i in range(self.getMaxChnls(self.outputIndexes[0])):
            self.outputChnls.append(str(i+1))

        label = wx.StaticText(self, -1, "Number of inputs:")
        self.noInChnls = wx.ComboBox(self, -1, self.inputChnls[0], choices=self.inputChnls, style=wx.CB_DROPDOWN)
        
        boxChnl.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        boxChnl.Add(self.noInChnls, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        
        label = wx.StaticText(self, -1, "Number of outputs:")
        self.noOutChnls = wx.ComboBox(self, -1, self.outputChnls[0], choices=self.outputChnls, style=wx.CB_DROPDOWN)
        
        boxChnl.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        boxChnl.Add(self.noOutChnls, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
              
        sizer.Add(boxAU, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        sizer.Add(boxChnl, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)    
        
        line = wx.StaticLine(self, -1, size=(20,-1), style=wx.LI_HORIZONTAL)
        sizer.Add(line, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.TOP, 5)
        
        self.radioOpen = wx.RadioButton(self, -1, "Open existing session")
        sizer.Add(self.radioOpen, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        
        label = wx.StaticText(self, -1, "Path:")
        self.path = wx.TextCtrl(self, -1, value="", style=wx.TE_PROCESS_ENTER)
        self.chooseBtn = wx.Button(self, label="Choose...", style=wx.BU_EXACTFIT)
        boxOpen.Add(label, 0, wx.ALIGN_CENTRE|wx.ALL, 5)
        boxOpen.Add(self.path, 1, wx.ALIGN_CENTRE|wx.ALL, 5) 
        boxOpen.Add(self.chooseBtn, 0, wx.ALIGN_CENTRE|wx.ALL, 5) 
        sizer.Add(boxOpen, 0, wx.GROW|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5)
        
        # Ok Button
        btnsizer = wx.StdDialogButtonSizer()
        okBtn = wx.Button(self, wx.ID_OK)
        okBtn.SetDefault()
        btnsizer.AddButton(okBtn)
        btnsizer.Realize()
        sizer.Add(okBtn, 0, wx.ALIGN_CENTER|wx.ALL, 5)
        
        self.radioNew.SetValue(1)

        self.SetSizer(sizer)
        sizer.Fit(self)
        
        self.chooseBtn.Bind(wx.EVT_BUTTON, self.chooseSessionFile)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.au.Bind(wx.EVT_COMBOBOX, self.AUModified)

    def OnClose(self, e):
        try:
            self.Destroy()
        except:
            pass
        raise SystemExit
        
    def AUModified(self, e):
        # Input Chnls
        value = self.noInChnls.GetSelection()
        if value == -1:
            value = 0
        self.noInChnls.Clear()
        indexes = self.getDevicesIndexesFromString()
        choices = self.getMaxChnls(indexes[0])
        self.inputChnls = []
        for i in range(choices):
            self.inputChnls.append(str(i+1))
            self.noInChnls.Append(self.inputChnls[i])
        if value >= len(self.inputChnls):
            self.noInChnls.SetSelection(len(self.inputChnls)-1)
        else:
            self.noInChnls.SetSelection(value)
            
        # Output Chnls
        outValue = self.noOutChnls.GetSelection()
        if outValue == -1:
            outValue = 0
        self.noOutChnls.Clear()
        choices = self.getMaxChnls(indexes[1])
        self.outputChnls = []
        for i in range(choices):
            self.outputChnls.append(str(i+1))
            self.noOutChnls.Append(self.outputChnls[i])
        if outValue >= len(self.outputChnls):
            self.noOutChnls.SetSelection(len(self.outputChnls)-1)
        else:
            self.noOutChnls.SetSelection(outValue)
        
# Liste des drivers ne s'update pas: on doit redemarrer le programme pour updater la liste       
    def getAvailableAudioMidiDrivers(self):
        inputDriverList, inputDriverIndexes = pa_get_input_devices()
        defaultInputDriver = inputDriverList[inputDriverIndexes.index(pa_get_default_input())]
        outputDriverList, outputDriverIndexes = pa_get_output_devices()
        defaultOutputDriver = outputDriverList[outputDriverIndexes.index(pa_get_default_output())]
        midiDriverList, midiDriverIndexes = pm_get_input_devices()
        if midiDriverList == []:
            defaultMidiDriver = ""
        else:
            defaultMidiDriver = midiDriverList[midiDriverIndexes.index(pm_get_default_input())]
        return inputDriverList, inputDriverIndexes, defaultInputDriver, outputDriverList, outputDriverIndexes, \
                defaultOutputDriver, midiDriverList, midiDriverIndexes, defaultMidiDriver
        
    def getDevicesIndexesFromString(self):
        # Extraire les noms des drivers de la grosse chaine
        val = self.au.GetValue()
        end = self.au.GetValue().find('/')
        start = end + 1
        
        return self.inputIndexes[self.inputs.index(val[0:end])], self.outputIndexes[self.outputs.index(val[start:])]
        
    def getDevicesIndexesAndMaxChannels(self, inputDriver, outputDriver, maxInChnls, maxOutChnls):
        inputDriverList, inputDriverIndexes = pa_get_input_devices()
        outputDriverList, outputDriverIndexes = pa_get_output_devices()
        realMaxIn = realMaxOut = 0
        # In case if the saved device is no longer connected to the PC
        if inputDriver in inputDriverList:
            audioInDevice = inputDriverIndexes[inputDriverList.index(inputDriver)]
            realMaxIn = maxInChnls
        else:
            audioInDevice = pa_get_default_input()
            realMaxIn = pa_get_input_max_channels(audioInDevice)
            print realMaxIn
        if outputDriver in outputDriverList:
            audioOutDevice = outputDriverIndexes[outputDriverList.index(outputDriver)]
            realMaxOut = maxOutChnls
        else:
            audioOutDevice = pa_get_default_output()
            realMaxOut = pa_get_output_max_channels(audioOutDevice)
#        noMidiDevices = pm_count_devices()
#        if midiInDevice >= noMidiDevices:
#            midiInDevice = pm_get_default_input()
        return audioInDevice, audioOutDevice, realMaxIn, realMaxOut
        
    def getMaxChnls(self, input):
        return pa_get_input_max_channels(input)
        
    def getRealMaxChnls(self, input, output, maxInChnls, maxOutChnls):
        if pa_get_input_max_channels(input) < maxInChnls:
            inChnls = pa_get_input_max_channels(input)
        else:
            inChnls = maxInChnls
        if pa_get_output_max_channels(output) < maxOutChnls:
            outChnls = pa_get_output_max_channels(output)
        else:
            outChnls = maxOutChnls
        return inChnls, outChnls
        
    def chooseSessionFile(self, e):
        wildcard = "Save file (*.txt)|*.txt|"        \
           "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, message="Open session ...", defaultDir=cGetSaveDir(), 
            defaultFile="", wildcard=wildcard, style=wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            self.path.SetValue(dlg.GetPath())
        dlg.Destroy()
        self.FitInside()  
        
class MainFrame(wx.Frame):
    def __init__(self, parent, ID=-1, title="MESA Instrument", pos=wx.DefaultPosition, 
                 size=wx.DefaultSize, style=wx.DEFAULT_FRAME_STYLE, noInputs=2, noOutputs=2, midiCtls=None):
        wx.Frame.__init__(self, parent, ID, title, pos, size, style)

        self._noInputs = noInputs
        self._noOutputs = noOutputs
        self._inputs = []
        self._outputs = []
        self.midiscanning = False
        self.midiCtls = {}
        self.pendingMidiObjects = []
            
        font = self.GetFont()
        self.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.NORMAL, False, 'Trebuchet MS'))
    
        # Barre de menu
        file_menu = wx.Menu()    
        file_menu.Append(wx.ID_ABOUT, '&About', 'Information about this application')
        file_menu.AppendSeparator()
        file_menu.Append(wx.ID_PREFERENCES, '&Preferences...', "Setup the application's preferences")
        file_menu.AppendSeparator()
        file_menu.Append(wx.ID_SAVE, '&Save Session...', 'Save the current session')
        file_menu.AppendSeparator()
        file_menu.Append(wx.ID_EXIT, '&Exit', 'Exit the application')
        
        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, '&File')
        self.SetMenuBar(menu_bar)
        
        # Sizers
        displaySize = wx.DisplaySize()
        self.sizerMain = wx.BoxSizer(wx.VERTICAL)
        self.sizerInsOuts = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerInputs = wx.BoxSizer(wx.VERTICAL)
        self.sizerOutputs = wx.BoxSizer(wx.VERTICAL)
        self.subSizerOutputs = wx.BoxSizer(wx.HORIZONTAL)
        self.subSizerButton = wx.BoxSizer(wx.HORIZONTAL)
        
        self.insOutsPanel = wx.Panel(self)
        
        # Inputs Section
        self.inputsPanel = wx.ScrolledWindow(self.insOutsPanel)
        for i in range(self._noInputs):
            self._inputs.append(InputPanel(self.inputsPanel, inputNo=i))
            self.sizerInputs.Add((5,5))
            self.sizerInputs.Add(self._inputs[i], flag=wx.GROW, proportion=0)
        
        # Generators
        self.sizerInputs.Add((5,5))
        self.sizerInputs.Add((1,-1), proportion=1)
        self.sizerInputs.Add((5,5))
        self._sineGen = SinePanel(self.inputsPanel)
        self.sizerInputs.Add(self._sineGen, flag=wx.GROW, proportion=0)
        self.sizerInputs.Add((5,5))
        self._noiseGen = NoisePanel(self.inputsPanel)
        self.sizerInputs.Add(self._noiseGen, flag=wx.GROW, proportion=0)
        self.sizerInputs.Add((5,5))
        
        self.inputsPanel.SetSizer(self.sizerInputs)
        self.inputsPanel.SetScrollbars(5, 5, 50, 50, 0, 0, False)
        
        # Outputs section
        self.outputsPanel = wx.ScrolledWindow(self.insOutsPanel)
        for i in range(self._noOutputs):
            self._outputs.append(OutputPanel(self.outputsPanel, outputNo=i))
            self.subSizerOutputs.Add((5,5))
            self.subSizerOutputs.Add(self._outputs[i], flag=wx.GROW, proportion=0)
            
        self._muteAllButton = wx.ToggleButton(self.outputsPanel, -1, label='Mute All', style=wx.ALIGN_CENTER, name='MuteAll')
        self.subSizerButton.Add((5,5))
        self.subSizerButton.Add(self._muteAllButton, proportion=1)
        self.subSizerButton.Add((5,5))
        
        self.sizerOutputs.Add(self.subSizerOutputs, flag=wx.GROW, proportion=1)
        self.sizerOutputs.Add(self.subSizerButton, flag=wx.GROW)
        self.sizerOutputs.Add((5,10))
        self.outputsPanel.SetSizer(self.sizerOutputs)
        self.outputsPanel.SetScrollbars(5, 5, 50, 50, 0, 0, False)
        
        #MasterFX section
        self.masterFxPanel = MasterFXPanel(self)
        
        # Ins and Outs sizer set
        self.sizerInsOuts.Add(self.inputsPanel, flag=wx.EXPAND, proportion=2)
        self.sizerInsOuts.Add(self.outputsPanel, flag=wx.EXPAND, proportion=1)
        self.insOutsPanel.SetSizerAndFit(self.sizerInsOuts)
        
        # Main sizer set
        self.sizerMain.Add(self.insOutsPanel, flag=wx.EXPAND, proportion=1)
        self.sizerMain.Add(self.masterFxPanel, flag=wx.EXPAND, proportion=0)
        self.SetSizerAndFit(self.sizerMain)
        self.SetTitle(cGetSessionName()+' - MESA Instrument')
        # Ajustements taille et position de la fenêtre
        wx.CallAfter(self.SetSize,(displaySize[0]/1.3,displaySize[1]/1.8))
        wx.CallAfter(self.CenterOnScreen)
        self.SetFocus()
        
        # Menu Events
        self.Bind(wx.EVT_MENU, self.aboutMenu, id=wx.ID_ABOUT)
        self.Bind(wx.EVT_MENU, self.saveSession, id=wx.ID_SAVE)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.Bind(wx.EVT_MENU, self.OnClose, id=wx.ID_EXIT)
        
        self._muteAllButton.Bind(wx.EVT_TOGGLEBUTTON, self.onMuteAllButton)
        self._muteAllButton.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        
        if midiCtls != None:
            if len(midiCtls.items()) > 0:
                self.assignAllMidiCtls(midiCtls)

    def onMouseRightDown(self, evt):
        if evt.ShiftDown():
            if evt.AltDown():
                o = evt.GetEventObject().GetName()
                try:
                    ctlnum = self.midiCtls[o]
                    # We delete all ctls associated with the parameter
                    for c in ctlnum:
                        if o == "MuteAll":
                            cGetMidiServer().unbind("ctls", c, self.midiMuteAll)
                    self.setMidiCtl(None, o)
                    del self.midiCtls[o]
                    sRevertMidiBackgroundColour(evt.GetEventObject())
                    return
                except KeyError:
                    pass
            if not self.midiscanning:
                print "1"
                self.midiscanning = True
                self.pendingMidiObjects.append(evt.GetEventObject().GetName())
                cGetMidiServer().ctlscan(self.getMidiScan)
       
    def saveSession(self, e):
        wildcard = "Save file (*.txt)|*.txt|"        \
           "All files (*.*)|*.*"
        dlg = wx.FileDialog(self, message="Save session as ...", defaultDir=cGetSaveDir(), 
            defaultFile=cGetSessionName(), wildcard=wildcard, style=wx.SAVE)
            
        if dlg.ShowModal() == wx.ID_OK:
            fn = dlg.GetFilename()
            dir = dlg.GetDirectory()
            self.SetTitle(fn+' - MESA Instrument')
            ctls = self.getAllMidiCtls()
            cSaveSession(fn, dir, ctls)
        dlg.Destroy()
        
    def aboutMenu(self, e):
        info = wx.AboutDialogInfo()
        info.Name = "MESA Instrument"
        info.Version = "3.1.0"
        info.Copyright = u"Université de Montréal - 2017"
        info.Description = wordwrap(
            u"This software instrument was conceived\nfor "
            u"Jonathan Goldman, professor at \nthe Université de Montréal. It was created \nfor the 2017 performance "
            u"of 'MESA',\nby Gordon Mumma.",
            350, wx.ClientDC(self))
        info.Developers = [ u"Francis Lecavalier"]
        wx.AboutBox(info)
        
    def OnClose(self, e):
        try:
            self.Destroy()
        except:
            pass
        raise SystemExit
        
    def onMuteAllButton(self, e):
        state = e.GetInt()
        cToggleMuteAll(state)
        self.Refresh()
        
    def midiMuteAll(self, value):
        newValue = rescale(value, 0, 127, 0, 1)
        cToggleMuteAll(newValue)
        wx.CallAfter(self._muteAllButton.SetValue(newValue))
#        self.Layout()
        
    def getMidiScan(self, ctlnum, midichnl):
        print "2"
        self.assignMidiCtl(ctlnum)
        
    def assignMidiCtl(self, ctlnum, object=None):
        if object != None:
            self.pendingMidiObjects.append(object)
        for o in self.pendingMidiObjects:
            self.setMidiCtl(ctlnum, o)
            if o == "MuteAll":
                cGetMidiServer().bind("ctls", ctlnum, self.midiMuteAll)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self._muteAllButton)
        self.pendingMidiObjects = []
        if self.midiscanning == True:
            self.midiscanning = False
            cGetMidiServer().ctlscan(None)
        
    def setMidiCtl(self, x, objectName):
        if objectName in self.midiCtls:
            array = []
            if type(self.midiCtls[objectName]) is list:
                array = (self.midiCtls[objectName])
            else:
                array.append(self.midiCtls[objectName])
            array.append(x)
            self.midiCtls[objectName] = array
        else:
            self.midiCtls[objectName] = [x]
        wx.CallAfter(self.Refresh)
        
    def getAllMidiCtls(self):
        temp = {}
        temp['Main'] = self.getMidiCtls()
        for i in self._inputs:
            temp['In '+str(i._inputNo)] = i.getMidiCtls()
        for o in self._outputs:
            temp['Out '+str(o._outputNo)] = o.getMidiCtls()
        temp['Sine'] = self._sineGen.getMidiCtls()
        temp['Noise'] = self._noiseGen.getMidiCtls()
        temp['MasterFX'] = self.masterFxPanel.getMidiCtls()
        return temp
        
    def getMidiCtls(self):
        temp = {}
        for k in self.midiCtls:
            temp[k] = self.midiCtls.get(k, 0)
        return temp
        
    def assignAllMidiCtls(self, midiCtls):
        for k in midiCtls.keys():
            if k == 'Main':
                section = self
            elif k == "Sine":
                section = self._sineGen
            elif k == 'Noise':
                section = self._noiseGen
            elif k[:3] == "In ":
                section = self._inputs[int(k[3:])]
            elif k[:3] == "Out":
                section = self._outputs[int(k[4:])]
            elif k == "MasterFX":
                section = self.masterFxPanel
                
            for i in midiCtls[k].keys():
                for j in range(len(midiCtls[k][i])):
                    section.assignMidiCtl(midiCtls[k][i][j], i)
                    j += 1
            
        self.Layout()

#    def addInput(self):
#        self._noInputs += 1
#        
#        self._inputs.append(InputPanel(self.inputsPanel, inputNo=(self._noInputs-1)))
#        self.sizerInputs.Add((5,5))
#        self.sizerInputs.Add(self._inputs[-1], flag=wx.GROW, proportion=0)
#        self.sizerInputs.Layout()
#        self.inputsPanel.FitInside()

#        if len(midiCtls.items()) > 0:
#            for k in midiCtls.keys():
#                if k == "Volume" or k == "Pan" or k == "Mute" or k == "Solo" or k == "Play":
#                    panel = self.tracks[-1].panelVol
#                elif k == "Playback" or k == "Gate" or k == "Reverse" or k == "Random" or k == "Size":
#                    panel = self.tracks[-1].panelSample
#                if type(midiCtls[k]) is list:
#                    for i in range(len(midiCtls[k])):
#                        panel.assignMidiCtl(midiCtls[k][i], k)
#                        i += 1
#                else:
#                    panel.assignMidiCtl(midiCtls[k], k)

#    def addOutput(self):
#        self._noOutputs += 1
#        
#        self._outputs.append(OutputPanel(self.outputsPanel, outputNo=(self._noOutputs-1)))
#        self.sizerOutputs.Add((5,5))
#        self.sizerOutputs.Add(self._outputs[-1], flag=wx.GROW, proportion=0)
#        self.sizerOutputs.Layout()
#        self.outputsPanel.FitInside()

class InputPanel(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=wx.RIGHT, name="Input Panel", inputNo=0):
        wx.Panel.__init__(self, parent, ID, pos, size, style, name)
        
        self._inputNo = inputNo
        self._parent = parent
        self.midiscanning = False
        self.midiCtls = {}
        self.pendingMidiObjects = []
        
        self.filterPanel = wx.Panel(self)
        
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerFilter = wx.GridBagSizer(0,0)
        
        self.text = wx.StaticText(self, label="In %d"%(inputNo+1), size=wx.DefaultSize, style=wx.ALIGN_CENTER)
        font = self.text.GetFont()
        font.SetPointSize(30)
        self.text.SetFont(font)
        
        # Elements defs.
        self.bypassButton = wx.ToggleButton(self.filterPanel, -1, label='BP Bypass', style=wx.ALIGN_CENTER, name='Bypass')
        self.freqKnob = QLiveControlKnob(self.filterPanel, 200, 5000, cGetFilterFreq(self._inputNo), size=(60,60), knobRadius=40, label="Freq.", outFunction=self.filterFreqKnobMove, name="FreqKnob")
        self.qKnob = QLiveControlKnob(self.filterPanel, 1, 10, cGetFilterQ(self._inputNo), size=(60,60), knobRadius=40, label="Q", outFunction=self.filterQKnobMove, name="QKnob")
        self.shiftKnob = QLiveControlKnob(self, -500.0, 500.0, cGetShift(self._inputNo), size=(60,80), knobRadius=45, label="Shift (Hz)", outFunction=self.shiftKnobMove, name="ShiftKnob")
#        self.slopeKnob = QLiveControlKnob(self, 0.001, 30, cGetSlope(self._inputNo), size=(60,80), knobRadius=45, label="Slope", outFunction=self.slopeKnobMove, name="SlopeKnob")
        self.gainKnob = QLiveControlKnob(self, 0.05, 10, cGetInputGain(self._inputNo), size=(60,80), knobRadius=45, label="Gain", outFunction=self.gainKnobMove, name="GainKnob")

        # Elements initialization
        self.carrierButton = wx.ToggleButton(self, -1, label='C', style=wx.BU_EXACTFIT|wx.ALIGN_CENTER, name='Carrier')
        self.modulatorButton = wx.ToggleButton(self, -1, label='M', style=wx.BU_EXACTFIT|wx.ALIGN_CENTER, name='Modulator')
        font = self.carrierButton.GetFont()
        font.SetPointSize(18)
        self.carrierButton.SetFont(font)
        self.modulatorButton.SetFont(font)
        
        self.bypassButton.SetValue(cIsFilterBypassed(self._inputNo))
        self.carrierButton.SetValue(cIsCarrier(self._inputNo))
        self.modulatorButton.SetValue(cIsModulator(self._inputNo))
        
        self.vuMeter = PyoGuiVuMeter(self, 1)
        cGetInputPeak(self._inputNo).setFunction(self.vuMeter.setRms)
        
        # Sizer adds
        self.sizerFilter.Add(self.bypassButton, pos=(0,0), span=(1,2), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.sizerFilter.Add(self.freqKnob, pos=(1,0), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.sizerFilter.Add(self.qKnob, pos=(1,1), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.filterPanel.SetSizerAndFit(self.sizerFilter)

        self.sizer.Add((5,5))
        self.sizer.Add(self.text, flag=wx.ALIGN_CENTER)
        self.sizer.Add((29,5))
        self.sizer.Add(self.filterPanel, flag=wx.ALIGN_CENTER)
        self.sizer.Add((5,5))
        self.sizer.Add(self.shiftKnob, flag=wx.ALIGN_CENTER)
        self.sizer.Add((5,5))
#        self.sizer.Add(self.slopeKnob, flag=wx.ALIGN_CENTER)
#        self.sizer.Add((5,5))
        self.sizer.Add(self.carrierButton, flag=wx.ALIGN_CENTER)
        self.sizer.Add((5,5))
        self.sizer.Add(self.modulatorButton, flag=wx.ALIGN_CENTER)
        self.sizer.Add((10,5))
        self.sizer.Add(self.vuMeter, flag=wx.ALIGN_CENTER, proportion=1)
        self.sizer.Add((20,5))
        self.sizer.Add(self.gainKnob, flag=wx.ALIGN_CENTER)
        self.sizer.Add((5,5))
        self.SetSizerAndFit(self.sizer)
        
        # Event Binds
        # Toggle buttons
        self.bypassButton.Bind(wx.EVT_TOGGLEBUTTON, self.onBypassButton) 
        self.carrierButton.Bind(wx.EVT_TOGGLEBUTTON, self.onCarrierButton) 
        self.modulatorButton.Bind(wx.EVT_TOGGLEBUTTON, self.onModulatorButton) 
        # Double-clicks
        self.freqKnob.Bind(wx.EVT_LEFT_DCLICK, self.freqKnobReset)
        self.qKnob.Bind(wx.EVT_LEFT_DCLICK, self.qKnobReset)
        self.shiftKnob.Bind(wx.EVT_LEFT_DCLICK, self.shiftKnobReset)
#        self.slopeKnob.Bind(wx.EVT_LEFT_DCLICK, self.slopeKnobReset)
        self.gainKnob.Bind(wx.EVT_LEFT_DCLICK, self.gainKnobReset)
        # Right-clicks
        self.bypassButton.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.freqKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.qKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
#        self.slopeKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.shiftKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.carrierButton.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.modulatorButton.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.gainKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)

    def onMouseRightDown(self, evt):
        if evt.ShiftDown():
            if evt.AltDown():
                o = evt.GetEventObject().GetName()
                try:
                    ctlnum = self.midiCtls[o]
                    # We delete all ctls associated with the parameter
                    for c in ctlnum:
                        if o == "Bypass":
                            cGetMidiServer().unbind("ctls", c, self.midiBypass)
                        elif o == "FreqKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiFreqKnob)
                        elif o == "QKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiQKnob)
                        elif o == "ShiftKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiShiftKnob)
                        elif o == "SlopeKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiSlopeKnob)
                        elif o == "GainKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiGainKnob)
                        elif o == "Carrier":
                            cGetMidiServer().unbind("ctls", c, self.midiCarrier)
                        elif o == "Modulator":
                            cGetMidiServer().unbind("ctls", c, self.midiModulator)
                    self.setMidiCtl(None, o)
                    del self.midiCtls[o]
                    sRevertMidiBackgroundColour(evt.GetEventObject())
                    return
                except KeyError:
                    pass
            if not self.midiscanning:
                self.midiscanning = True
                self.pendingMidiObjects.append(evt.GetEventObject().GetName())
                cGetMidiServer().ctlscan(self.getMidiScan)

    def filterFreqKnobMove(self, value):
        cSetFilterFreq(self._inputNo, value)
        self.Refresh()
        
    def gainKnobMove(self, value):
        cSetInputGain(self._inputNo, value)
        self.Refresh()
        
    def filterQKnobMove(self, value):
        cSetFilterQ(self._inputNo, value)
        self.Refresh()
        
    def shiftKnobMove(self, value):
        cSetShift(self._inputNo, value)
        self.Refresh()
    
    def slopeKnobMove(self, value):
        cSetSlope(self._inputNo, value)
        self.Refresh()
        
    def onBypassButton(self, evt):
        state = evt.GetInt()
        cToggleBypass(self._inputNo, state)
        self.Refresh()
        
    def onCarrierButton(self, evt):
        state = evt.GetInt()
        topParent = self._parent.GetParent().GetParent()
        for i in range(len(topParent._inputs)):
            if topParent._inputs[i].carrierButton.GetValue() == 1 and i != self._inputNo:
                topParent._inputs[i].carrierButton.SetValue(0)
        if topParent._sineGen.carrierButton.GetValue() == 1:
            topParent._sineGen.carrierButton.SetValue(0)
        if topParent._noiseGen.carrierButton.GetValue() == 1:
            topParent._noiseGen.carrierButton.SetValue(0)
        cToggleCarrier(self._inputNo, state)
        self.Refresh()
        
    def onModulatorButton(self, evt):
        state = evt.GetInt()
        topParent = self._parent.GetParent().GetParent()
        for i in range(len(topParent._inputs)):
            if topParent._inputs[i].modulatorButton.GetValue() == 1 and i != self._inputNo:
                topParent._inputs[i].modulatorButton.SetValue(0)
        if topParent._sineGen.modulatorButton.GetValue() == 1:
            topParent._sineGen.modulatorButton.SetValue(0)
        if topParent._noiseGen.modulatorButton.GetValue() == 1:
            topParent._noiseGen.modulatorButton.SetValue(0)
        cToggleModulator(self._inputNo, state)
        self.Refresh()
        
    def freqKnobReset(self, e):
        value = 1000.0
        e.GetEventObject().SetValue(value)
        self.filterFreqKnobMove(value)
        self.Refresh()
        
    def qKnobReset(self, e):
        value = 1.0
        e.GetEventObject().SetValue(value)
        self.filterQKnobMove(value)
        self.Refresh()
        
    def shiftKnobReset(self, e):
        value = 0.00
        e.GetEventObject().SetValue(value)
        self.shiftKnobMove(value)
        self.Refresh()

    def slopeKnobReset(self, e):
        value = 0.01
        e.GetEventObject().SetValue(value)
        self.slopeKnobMove(value)
        self.Refresh()
        
    def gainKnobReset(self, e):
        value = 1.0
        e.GetEventObject().SetValue(value)
        self.gainKnobMove(value)
        self.Refresh()
            
    def midiBypass(self, value):
        newValue = rescale(value, 0, 127, 0, 1)
        cToggleBypass(self._inputNo, newValue)
        wx.CallAfter(self.bypassButton.SetValue(newValue))
#        self.Layout()
        
    def midiFreqKnob(self, value):
        newValue = rescale(value, 0, 127, 200.0, 5000.0)
        cSetFilterFreq(self._inputNo, newValue)
        wx.CallAfter(self.freqKnob.SetValue(newValue))
#        self.Layout()
        
    def midiQKnob(self, value):
        newValue = rescale(value, 0, 127, 1.0, 10.0)
        cSetFilterQ(self._inputNo, newValue)
        wx.CallAfter(self.qKnob.SetValue(newValue))
#        wx.CallAfter(self.Layout())
    
    def midiShiftKnob(self, value):
        newValue = rescale(value, 0, 127, -500.0, 500.0)
        cSetShift(self._inputNo, newValue)
        wx.CallAfter(self.shiftKnob.SetValue(newValue))
#        self.Layout()

    def midiSlopeKnob(self, value):
        newValue = rescale(value, 0, 127, 0.001, 30.0)
        cSetSlope(self._inputNo, newValue)
        wx.CallAfter(self.slopeKnob.SetValue(newValue))
        self.Layout()
        
    def midiCarrier(self, value):
#        newValue = rescale(value, 0, 127, 0, 1)
        oldVal = self.carrierButton.GetValue()
        if oldVal == 0:
            newValue = 1
        else:
            newValue = 0
        topParent = self._parent.GetParent().GetParent()
        if newValue == 1:
            for i in range(len(topParent._inputs)):
                if topParent._inputs[i].carrierButton.GetValue() == 1 and i != self._inputNo:
                    topParent._inputs[i].carrierButton.SetValue(0)
            if topParent._sineGen.carrierButton.GetValue() == 1:
                topParent._sineGen.carrierButton.SetValue(0)
            if topParent._noiseGen.carrierButton.GetValue() == 1:
                topParent._noiseGen.carrierButton.SetValue(0)
        cToggleCarrier(self._inputNo, newValue)
        wx.CallAfter(self.carrierButton.SetValue(newValue))
        self.Layout()
        
    def midiModulator(self, value):
#        newValue = rescale(value, 0, 127, 0, 1)
        oldVal = self.modulatorButton.GetValue()
        if oldVal == 0:
            newValue = 1
        else:
            newValue = 0
        topParent = self._parent.GetParent().GetParent()
        if newValue == 1:
            for i in range(len(topParent._inputs)):
                if topParent._inputs[i].modulatorButton.GetValue() == 1 and i != self._inputNo:
                    topParent._inputs[i].modulatorButton.SetValue(0)
            if topParent._sineGen.modulatorButton.GetValue() == 1:
                topParent._sineGen.modulatorButton.SetValue(0)
            if topParent._noiseGen.modulatorButton.GetValue() == 1:
                topParent._noiseGen.modulatorButton.SetValue(0)
        cToggleModulator(self._inputNo, newValue)
        wx.CallAfter(self.modulatorButton.SetValue(newValue))
        self.Layout()
            
    def midiGainKnob(self, value):
        newValue = rescale(value, 0, 127, 0.05, 10.0)
        cSetInputGain(self._inputNo, newValue)
        wx.CallAfter(self.gainKnob.SetValue(newValue))
#        self.Layout()
        
    def getMidiScan(self, ctlnum, midichnl):
        self.assignMidiCtl(ctlnum)
        
    def assignMidiCtl(self, ctlnum, object=None):
        if object != None:
            self.pendingMidiObjects.append(object)
        for o in self.pendingMidiObjects:
            self.setMidiCtl(ctlnum, o)
            if o == "Bypass":
                cGetMidiServer().bind("ctls", ctlnum, self.midiBypass)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.bypassButton)
            elif o == "FreqKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiFreqKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.freqKnob)
            elif o == "QKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiQKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.qKnob)
            elif o == "Carrier":
                cGetMidiServer().bind("ctls", ctlnum, self.midiCarrier)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.carrierButton)
            elif o == "Modulator":
                cGetMidiServer().bind("ctls", ctlnum, self.midiModulator)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.modulatorButton)
            elif o == "ShiftKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiShiftKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.shiftKnob)
            elif o == "SlopeKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiSlopeKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.slopeKnob)
            elif o == "GainKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiGainKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.gainKnob)
        self.pendingMidiObjects = []
        if self.midiscanning == True:
            self.midiscanning = False
            cGetMidiServer().ctlscan(None)
        
    def setMidiCtl(self, x, objectName):
        # This is because a parameter can have multiple midi ctls. They are stored in a list, inside the midiCtls dictionnary.
        if objectName in self.midiCtls:
            array = []
            if type(self.midiCtls[objectName]) is list:
                array = (self.midiCtls[objectName])
            else:
                array.append(self.midiCtls[objectName])
            array.append(x)
            self.midiCtls[objectName] = array
        else:
            self.midiCtls[objectName] = [x]
        wx.CallAfter(self.Refresh)
        
    def getMidiCtls(self):
        temp = {}
        for k in self.midiCtls:
            temp[k] = self.midiCtls.get(k, 0)
        return temp
        
#    def deleteInput(self):
#        topParent = self._parent.GetParent()
#        
#        topParent.sizerInputs.Hide(topParent._inputs[self._inputNo])
#        # To delete the associated (5,5) spacer
#        if self._inputNo != 0:
#            topParent.sizerInputs.Remove((topParent._noInputs*2)-((topParent._noInputs-self._inputNo)*2))
#        else:
#            topParent.sizerInputs.Remove(0)       
#        topParent.sizerInputs.Remove(topParent._inputs[self._inputNo])
#        topParent._inputs[self._inputNo].Destroy
#        del topParent._inputs[self._inputNo]
#        
#        topParent._noInputs -= 1
#        # To delete remaining spacers
#        if topParent._noInputs == 0:
#            topParent.sizerInputs.Clear()
#        self._parent.Layout()
#        self._parent.FitInside()
#        topParent.Layout()
#        topParent.FitInside()
        
class SinePanel(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=wx.RIGHT, name="Sine Panel"):
        wx.Panel.__init__(self, parent, ID, pos, size, style, name)
        
        self._parent = parent
        self.midiscanning = False
        self.midiCtls = {}
        self.pendingMidiObjects = []
        
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        
        self.text = wx.StaticText(self, label="Sine", size=wx.DefaultSize, style=wx.ALIGN_CENTER)
        font = self.text.GetFont()
        font.SetPointSize(30)
        self.text.SetFont(font)
        
        self.freqKnob = QLiveControlKnob(self, 200, 5000, cGetSineFreq(), size=(60,80), knobRadius=45, label="Freq.", outFunction=self.freqKnobMove, name="FreqKnob")
        self.gainKnob = QLiveControlKnob(self, 0.0001, 5.0, cGetSineGain(), size=(60,80), knobRadius=45, label="Gain", outFunction=self.gainKnobMove, name="GainKnob")

        self.carrierButton = wx.ToggleButton(self, -1, label='C', style=wx.BU_EXACTFIT|wx.ALIGN_CENTER, name='Carrier')
        self.modulatorButton = wx.ToggleButton(self, -1, label='M', style=wx.BU_EXACTFIT|wx.ALIGN_CENTER, name='Modulator')
        font = self.carrierButton.GetFont()
        font.SetPointSize(18)
        self.carrierButton.SetFont(font)
        self.modulatorButton.SetFont(font)
        
        self.carrierButton.SetValue(cIsCarrier(-2))
        self.modulatorButton.SetValue(cIsModulator(-2))
        
        self.vuMeter = PyoGuiVuMeter(self, 1)
        cGetSinePeak().setFunction(self.vuMeter.setRms)
        
        self.assignNoteOns()

        self.sizer.Add((5,5))
        self.sizer.Add(self.text, flag=wx.ALIGN_CENTER)
        self.sizer.Add((23,5))
        self.sizer.Add(self.freqKnob, flag=wx.ALIGN_CENTER)
        self.sizer.Add((130,5))
        self.sizer.Add(self.carrierButton, flag=wx.ALIGN_CENTER)
        self.sizer.Add((5,5))
        self.sizer.Add(self.modulatorButton, flag=wx.ALIGN_CENTER)
        self.sizer.Add((10,5))
        self.sizer.Add(self.vuMeter, flag=wx.ALIGN_CENTER, proportion=1)
        self.sizer.Add((20,5))
        self.sizer.Add(self.gainKnob, flag=wx.ALIGN_CENTER)
        self.sizer.Add((5,5))
        self.SetSizerAndFit(self.sizer)
        
        # Event Binds
        self.carrierButton.Bind(wx.EVT_TOGGLEBUTTON, self.onCarrierButton) 
        self.modulatorButton.Bind(wx.EVT_TOGGLEBUTTON, self.onModulatorButton) 
        self.freqKnob.Bind(wx.EVT_LEFT_DCLICK, self.freqKnobReset)
        self.gainKnob.Bind(wx.EVT_LEFT_DCLICK, self.gainKnobReset)
        self.freqKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.carrierButton.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.modulatorButton.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.gainKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)

    def onMouseRightDown(self, evt):
        if evt.ShiftDown():
            if evt.AltDown():
                o = evt.GetEventObject().GetName()
                try:
                    ctlnum = self.midiCtls[o]
                    # We delete all ctls associated with the parameter
                    for c in ctlnum:
                        if o == "FreqKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiFreqKnob)
                        elif o == "GainKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiGainKnob)
                        elif o == "Carrier":
                            cGetMidiServer().unbind("ctls", c, self.midiCarrier)
                        elif o == "Modulator":
                            cGetMidiServer().unbind("ctls", c, self.midiModulator)
                    self.setMidiCtl(None, o)
                    del self.midiCtls[o]
                    sRevertMidiBackgroundColour(evt.GetEventObject())
                    return
                except KeyError:
                    pass
            if not self.midiscanning:
                self.midiscanning = True
                self.pendingMidiObjects.append(evt.GetEventObject().GetName())
                cGetMidiServer().ctlscan(self.getMidiScan)

    def freqKnobMove(self, value):
        cSetSineFreq(value)
        self.Refresh()
        
    def gainKnobMove(self, value):
        cSetSineGain(value)
        self.Refresh()
        
    def onCarrierButton(self, evt):
        state = evt.GetInt()
        topParent = self._parent.GetParent().GetParent()
        for i in range(len(topParent._inputs)):
            if topParent._inputs[i].carrierButton.GetValue() == 1:
                topParent._inputs[i].carrierButton.SetValue(0)
        if topParent._noiseGen.carrierButton.GetValue() == 1:
                topParent._noiseGen.carrierButton.SetValue(0)
        cToggleCarrierSine(state)
        self.Refresh()
        
    def onModulatorButton(self, evt):
        state = evt.GetInt()
        topParent = self._parent.GetParent().GetParent()
        for i in range(len(topParent._inputs)):
            if topParent._inputs[i].modulatorButton.GetValue() == 1:
                topParent._inputs[i].modulatorButton.SetValue(0)
        if topParent._noiseGen.modulatorButton.GetValue() == 1:
                topParent._noiseGen.modulatorButton.SetValue(0)
        cToggleModulatorSine(state)
        self.Refresh()
        
    def freqKnobReset(self, e):
        value = 1000.0
        e.GetEventObject().SetValue(value)
        self.freqKnobMove(value)
        self.Refresh()
        
    def gainKnobReset(self, e):
        value = 1.0
        e.GetEventObject().SetValue(value)
        self.gainKnobMove(value)
        self.Refresh()
        
    def midiFreqKnob(self, value):
        newValue = rescale(value, 0, 127, 200.0, 5000.0)
        cSetSineFreq(newValue)
        wx.CallAfter(self.freqKnob.SetValue(newValue))
#        self.Layout()
        
    def midiGainKnob(self, value):
        newValue = rescale(value, 0, 127, 0.0001, 5.0)
        cSetSineGain(newValue)
        wx.CallAfter(self.gainKnob.SetValue(newValue))
#        self.Layout()
        
    def midiCarrier(self, value):
#        newValue = rescale(value, 0, 127, 0, 1)
        oldVal = self.carrierButton.GetValue()
        if oldVal == 0:
            newValue = 1
        else:
            newValue = 0
        topParent = self._parent.GetParent().GetParent()
        if newValue == 1:
            for i in range(len(topParent._inputs)):
                if topParent._inputs[i].carrierButton.GetValue() == 1:
                    topParent._inputs[i].carrierButton.SetValue(0)
            if topParent._noiseGen.carrierButton.GetValue() == 1:
                    topParent._noiseGen.carrierButton.SetValue(0)
        cToggleCarrierSine(newValue)
        wx.CallAfter(self.carrierButton.SetValue(newValue))
        self.Layout()
        
    def midiModulator(self, value):
#        newValue = rescale(value, 0, 127, 0, 1)
        oldVal = self.modulatorButton.GetValue()
        if oldVal == 0:
            newValue = 1
        else:
            newValue = 0
        topParent = self._parent.GetParent().GetParent()
        if newValue == 1:
            for i in range(len(topParent._inputs)):
                if topParent._inputs[i].modulatorButton.GetValue() == 1:
                    topParent._inputs[i].modulatorButton.SetValue(0)
            if topParent._noiseGen.modulatorButton.GetValue() == 1:
                    topParent._noiseGen.modulatorButton.SetValue(0)
        cToggleModulatorSine(newValue)
        wx.CallAfter(self.modulatorButton.SetValue(newValue))
        self.Layout()
        
    def noteOnFreqKnob(self, value, chnl):
        newValue = midiToHz(value)
        if newValue < 200:
            newValue = 200
        elif newValue > 5000:
            newValue = 5000
        cSetSineFreq(newValue)
        wx.CallAfter(self.freqKnob.SetValue(newValue))
#        self.Layout()
        
    def getMidiScan(self, ctlnum, midichnl):
        self.assignMidiCtl(ctlnum)
        
    def assignMidiCtl(self, ctlnum, object=None):
        if object != None:
            self.pendingMidiObjects.append(object)
        for o in self.pendingMidiObjects:
            self.setMidiCtl(ctlnum, o)
            if o == "FreqKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiFreqKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.freqKnob)
            elif o == "Carrier":
                cGetMidiServer().bind("ctls", ctlnum, self.midiCarrier)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.carrierButton)
            elif o == "Modulator":
                cGetMidiServer().bind("ctls", ctlnum, self.midiModulator)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.modulatorButton)
            elif o == "GainKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiGainKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.gainKnob)
        self.pendingMidiObjects = []
        if self.midiscanning == True:
            self.midiscanning = False
            cGetMidiServer().ctlscan(None)
        
    def setMidiCtl(self, x, objectName):
        # This is because a parameter can have multiple midi ctls. They are stored in a list, inside the midiCtls dictionnary.
        if objectName in self.midiCtls:
            array = []
            if type(self.midiCtls[objectName]) is list:
                array = (self.midiCtls[objectName])
            else:
                array.append(self.midiCtls[objectName])
            array.append(x)
            self.midiCtls[objectName] = array
        else:
            self.midiCtls[objectName] = [x]
        wx.CallAfter(self.Refresh)
        
    def getMidiCtls(self):
        temp = {}
        for k in self.midiCtls:
            temp[k] = self.midiCtls.get(k, 0)
        return temp
        
    # This is so a keyboard can change the sine pitch
    def assignNoteOns(self):
        for i in range(126):
            cGetMidiServer().bind("noteon", i, self.noteOnFreqKnob)

class NoisePanel(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=wx.RIGHT, name="Noise Panel"):
        wx.Panel.__init__(self, parent, ID, pos, size, style, name)
        
        self._parent = parent
        self.midiscanning = False
        self.midiCtls = {}
        self.pendingMidiObjects = []
        
        self.filterPanel = wx.Panel(self)
        
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerFilter = wx.GridBagSizer(0,0)
        
        self.text = wx.StaticText(self, label="Noise", size=wx.DefaultSize, style=wx.ALIGN_CENTER)
        font = self.text.GetFont()
        font.SetPointSize(30)
        self.text.SetFont(font)
        
        self.bypassButton = wx.ToggleButton(self.filterPanel, -1, label='BP Bypass', style=wx.ALIGN_CENTER, name='Bypass')
        self.freqKnob = QLiveControlKnob(self.filterPanel, 200, 5000, cGetNoiseFilterFreq(), size=(60,60), knobRadius=40, label="Freq.", outFunction=self.filterFreqKnobMove, name="FreqKnob")
        self.qKnob = QLiveControlKnob(self.filterPanel, 1, 10, cGetNoiseFilterQ(), size=(60,60), knobRadius=40, label="Q", outFunction=self.filterQKnobMove, name="QKnob")
        self.gainKnob = QLiveControlKnob(self, 0.05, 10, cGetNoiseGain(), size=(60,80), knobRadius=45, label="Gain", outFunction=self.gainKnobMove, name="GainKnob")

        self.carrierButton = wx.ToggleButton(self, -1, label='C', style=wx.BU_EXACTFIT|wx.ALIGN_CENTER, name='Carrier')
        self.modulatorButton = wx.ToggleButton(self, -1, label='M', style=wx.BU_EXACTFIT|wx.ALIGN_CENTER, name='Modulator')
        font = self.carrierButton.GetFont()
        font.SetPointSize(18)
        self.carrierButton.SetFont(font)
        self.modulatorButton.SetFont(font)
        
        self.bypassButton.SetValue(cIsNoiseFilterBypassed())
        self.carrierButton.SetValue(cIsCarrier(-3))
        self.modulatorButton.SetValue(cIsModulator(-3))
        
        self.vuMeter = PyoGuiVuMeter(self, 1)
        cGetNoisePeak().setFunction(self.vuMeter.setRms)
        
        self.sizerFilter.Add(self.bypassButton, pos=(0,0), span=(1,2), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.sizerFilter.Add(self.freqKnob, pos=(1,0), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.sizerFilter.Add(self.qKnob, pos=(1,1), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.filterPanel.SetSizerAndFit(self.sizerFilter)

        self.sizer.Add((5,5))
        self.sizer.Add(self.text, flag=wx.ALIGN_CENTER)
        self.sizer.Add((6,5))
        self.sizer.Add(self.filterPanel, flag=wx.ALIGN_CENTER)
        self.sizer.Add((70,5))
        self.sizer.Add(self.carrierButton, flag=wx.ALIGN_CENTER)
        self.sizer.Add((5,5))
        self.sizer.Add(self.modulatorButton, flag=wx.ALIGN_CENTER)
        self.sizer.Add((10,5))
        self.sizer.Add(self.vuMeter, flag=wx.ALIGN_CENTER, proportion=1)
        self.sizer.Add((20,5))
        self.sizer.Add(self.gainKnob, flag=wx.ALIGN_CENTER)
        self.sizer.Add((5,5))
        self.SetSizerAndFit(self.sizer)
        
        # Event Binds
        self.bypassButton.Bind(wx.EVT_TOGGLEBUTTON, self.onBypassButton) 
        self.carrierButton.Bind(wx.EVT_TOGGLEBUTTON, self.onCarrierButton) 
        self.modulatorButton.Bind(wx.EVT_TOGGLEBUTTON, self.onModulatorButton) 
        self.freqKnob.Bind(wx.EVT_LEFT_DCLICK, self.freqKnobReset)
        self.qKnob.Bind(wx.EVT_LEFT_DCLICK, self.qKnobReset)
        self.gainKnob.Bind(wx.EVT_LEFT_DCLICK, self.gainKnobReset)
        self.bypassButton.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.freqKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.qKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.carrierButton.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.modulatorButton.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.gainKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)

    def onMouseRightDown(self, evt):
        if evt.ShiftDown():
            if evt.AltDown():
                o = evt.GetEventObject().GetName()
                try:
                    ctlnum = self.midiCtls[o]
                    # We delete all ctls associated with the parameter
                    for c in ctlnum:
                        if o == "Bypass":
                            cGetMidiServer().unbind("ctls", c, self.midiBypass)
                        elif o == "FreqKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiFreqKnob)
                        elif o == "QKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiQKnob)
                        elif o == "GainKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiGainKnob)
                        elif o == "Carrier":
                            cGetMidiServer().unbind("ctls", c, self.midiCarrier)
                        elif o == "Modulator":
                            cGetMidiServer().unbind("ctls", c, self.midiModulator)
                    self.setMidiCtl(None, o)
                    del self.midiCtls[o]
                    sRevertMidiBackgroundColour(evt.GetEventObject())
                    return
                except KeyError:
                    pass
            if not self.midiscanning:
                self.midiscanning = True
                self.pendingMidiObjects.append(evt.GetEventObject().GetName())
                cGetMidiServer().ctlscan(self.getMidiScan)

    def filterFreqKnobMove(self, value):
        cSetNoiseFilterFreq(value)
        self.Refresh()
        
    def gainKnobMove(self, value):
        cSetNoiseGain(value)
        self.Refresh()
        
    def filterQKnobMove(self, value):
        cSetNoiseFilterQ(value)
        self.Refresh()
        
    def onBypassButton(self, evt):
        state = evt.GetInt()
        cToggleBypassNoise(state)
        self.Refresh()
        
    def onCarrierButton(self, evt):
        state = evt.GetInt()
        topParent = self._parent.GetParent().GetParent()
        for i in range(len(topParent._inputs)):
            if topParent._inputs[i].carrierButton.GetValue() == 1:
                topParent._inputs[i].carrierButton.SetValue(0)
        if topParent._sineGen.carrierButton.GetValue() == 1:
                topParent._sineGen.carrierButton.SetValue(0)
        cToggleCarrierNoise(state)
        self.Refresh()
        
    def onModulatorButton(self, evt):
        state = evt.GetInt()
        topParent = self._parent.GetParent().GetParent()
        for i in range(len(topParent._inputs)):
            if topParent._inputs[i].modulatorButton.GetValue() == 1:
                topParent._inputs[i].modulatorButton.SetValue(0)
        if topParent._sineGen.modulatorButton.GetValue() == 1:
                topParent._sineGen.modulatorButton.SetValue(0)
        cToggleModulatorNoise(state)
        self.Refresh()
        
    def freqKnobReset(self, e):
        value = 1000.0
        e.GetEventObject().SetValue(value)
        self.filterFreqKnobMove(value)
        self.Refresh()
        
    def qKnobReset(self, e):
        value = 1.0
        e.GetEventObject().SetValue(value)
        self.filterQKnobMove(value)
        self.Refresh()

    def gainKnobReset(self, e):
        value = 1.0
        e.GetEventObject().SetValue(value)
        self.gainKnobMove(value)
        self.Refresh()
        
    def midiBypass(self, value):
        newValue = rescale(value, 0, 127, 0, 1)
        cToggleBypassNoise(newValue)
        wx.CallAfter(self.bypassButton.SetValue(newValue))
#        self.Layout()
        
    def midiFreqKnob(self, value):
        newValue = rescale(value, 0, 127, 200.0, 5000.0)
        cSetNoiseFilterFreq(newValue)
        wx.CallAfter(self.freqKnob.SetValue(newValue))
#        self.Layout()
        
    def midiQKnob(self, value):
        newValue = rescale(value, 0, 127, 1.0, 10.0)
        cSetNoiseFilterQ(newValue)
        wx.CallAfter(self.qKnob.SetValue(newValue))
#        self.Layout()
            
    def midiGainKnob(self, value):
        newValue = rescale(value, 0, 127, 0.05, 10.0)
        cSetNoiseGain(newValue)
        wx.CallAfter(self.gainKnob.SetValue(newValue))
#        self.Layout()
        
    def midiCarrier(self, value):
#        newValue = rescale(value, 0, 127, 0, 1)
        oldVal = self.carrierButton.GetValue()
        if oldVal == 0:
            newValue = 1
        else:
            newValue = 0
        topParent = self._parent.GetParent().GetParent()
        if newValue == 1:
            for i in range(len(topParent._inputs)):
                if topParent._inputs[i].carrierButton.GetValue() == 1:
                    topParent._inputs[i].carrierButton.SetValue(0)
            if topParent._sineGen.carrierButton.GetValue() == 1:
                    topParent._sineGen.carrierButton.SetValue(0)
        cToggleCarrierNoise(newValue)
        wx.CallAfter(self.carrierButton.SetValue(newValue))
        self.Layout()
        
    def midiModulator(self, value):
#        newValue = rescale(value, 0, 127, 0, 1)
        oldVal = self.modulatorButton.GetValue()
        if oldVal == 0:
            newValue = 1
        else:
            newValue = 0
        topParent = self._parent.GetParent().GetParent()
        if newValue == 1:
            for i in range(len(topParent._inputs)):
                if topParent._inputs[i].modulatorButton.GetValue() == 1:
                    topParent._inputs[i].modulatorButton.SetValue(0)
            if topParent._sineGen.modulatorButton.GetValue() == 1:
                    topParent._sineGen.modulatorButton.SetValue(0)
        cToggleModulatorNoise(newValue)
        wx.CallAfter(self.modulatorButton.SetValue(newValue))
        self.Layout()
        
    def getMidiScan(self, ctlnum, midichnl):
        self.assignMidiCtl(ctlnum)
        
    def assignMidiCtl(self, ctlnum, object=None):
        if object != None:
            self.pendingMidiObjects.append(object)
        for o in self.pendingMidiObjects:
            self.setMidiCtl(ctlnum, o)
            if o == "Bypass":
                cGetMidiServer().bind("ctls", ctlnum, self.midiBypass)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.bypassButton)
            elif o == "FreqKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiFreqKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.freqKnob)
            elif o == "QKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiQKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.qKnob)
            elif o == "Carrier":
                cGetMidiServer().bind("ctls", ctlnum, self.midiCarrier)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.carrierButton)
            elif o == "Modulator":
                cGetMidiServer().bind("ctls", ctlnum, self.midiModulator)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.modulatorButton)
            elif o == "GainKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiGainKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.gainKnob)
        self.pendingMidiObjects = []
        if self.midiscanning == True:
            self.midiscanning = False
            cGetMidiServer().ctlscan(None)
        
    def setMidiCtl(self, x, objectName):
        # This is because a parameter can have multiple midi ctls. They are stored in a list, inside the midiCtls dictionnary.
        if objectName in self.midiCtls:
            array = []
            if type(self.midiCtls[objectName]) is list:
                array = (self.midiCtls[objectName])
            else:
                array.append(self.midiCtls[objectName])
            array.append(x)
            self.midiCtls[objectName] = array
        else:
            self.midiCtls[objectName] = [x]
        wx.CallAfter(self.Refresh)
        
    def getMidiCtls(self):
        temp = {}
        for k in self.midiCtls:
            temp[k] = self.midiCtls.get(k, 0)
        return temp
        
class OutputPanel(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=wx.RIGHT, name="Output Panel", outputNo=0):
        wx.Panel.__init__(self, parent, ID, pos, size, style, name)
        
        self._outputNo = outputNo
        self._parent = parent
        self.midiscanning = False
        self.midiCtls = {}
        self.pendingMidiObjects = []
        
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        
        self.text = wx.StaticText(self, label="Out %d"%(self._outputNo+1), size=wx.DefaultSize, style=wx.ALIGN_CENTER)
        font = self.text.GetFont()
        font.SetPointSize(20)
        self.text.SetFont(font)
        
        self.vumetre = PyoGuiVuMeter(self, nchnls=1, orient=wx.VERTICAL)
        self.volKnob = QLiveControlKnob(self, -45.0, 9.0, self.convertTodB(cGetOutputMul(self._outputNo)), size=(60,80), knobRadius=45, label="Vol. (dB)", outFunction=self.volKnobMove, name="VolumeKnob")
        cGetOutputPeak(self._outputNo).function = self.vumetre.setRms
        self.volValue = wx.StaticText(self, label="%d"%(0.0), style=wx.ALIGN_CENTER)
        font = self.volValue.GetFont()
        font.SetPointSize(10)
        self.volValue.SetFont(font)

#        self.slider = QLiveControlSlider(self, VOL_SLIDER_MIN, VOL_SLIDER_MAX, self.getRealVolumeValue(cGetOutputMul(self._outputNo), VOL_SLIDER_MIN, VOL_SLIDER_MAX),
#        size=(50,150), orient=wx.VERTICAL, log=True, outFunction=self.sliderMove, numSliders=1, name='Slider')
#        cGetOutputPeak(self._outputNo).function = self.slider.setRms

        self.sizer.Add(self.text, flag=wx.ALIGN_CENTER)
        self.sizer.Add(self.vumetre, flag=wx.ALIGN_CENTER, proportion=1)
        self.sizer.Add((5,5))
        self.sizer.Add(self.volValue, flag=wx.ALIGN_CENTER|wx.EXPAND, proportion=0)
        self.sizer.Add((5,5))
        self.sizer.Add(self.volKnob, flag=wx.ALIGN_CENTER|wx.EXPAND, proportion=0)
        self.sizer.Add((5,5))
        self.SetSizerAndFit(self.sizer)
        
        self.volKnob.Bind(wx.EVT_LEFT_DCLICK, self.volKnobReset)
        self.volKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        
        self.on_timer()
        
    def on_timer(self):
        amplitude = cGetOutputPeak(self._outputNo).get()
        db = self.convertTodB(amplitude)
#        db = self.convertTodB(math.log10(amplitude+0.00001) * 0.2 + 1.0)
        self.volValue.SetLabel("%.2f dB"%(db))
        wx.CallLater(100, self.on_timer)

    def onMouseRightDown(self, evt):
        if evt.ShiftDown():
            if evt.AltDown():
                o = evt.GetEventObject().GetName()
                try:
                    ctlnum = self.midiCtls[o]
                    # We delete all ctls associated with the parameter
                    for c in ctlnum:
                        if o == "VolumeKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiVolKnob)
                    self.setMidiCtl(None, o)
                    del self.midiCtls[o]
                    sRevertMidiBackgroundColour(evt.GetEventObject())
                    return
                except KeyError:
                    pass
            if not self.midiscanning:
                self.midiscanning = True
                self.pendingMidiObjects.append(evt.GetEventObject().GetName())
                cGetMidiServer().ctlscan(self.getMidiScan)

    def convertTodB(self, floatValue):
        try:
            return math.log10(floatValue) * 10
        except ValueError:
            return -120.0
    def convertToMul(self, floatValue):
        return pow(10, (floatValue/10))

    def volKnobMove(self, value):
        cSetOutputMul(self._outputNo, self.convertToMul(value))
        self.Refresh()
        
    def volKnobReset(self, e):
        value = 0.0
        e.GetEventObject().SetValue(value)
        self.volKnobMove(value)
        self.Refresh()
        
    def midiVolKnob(self, value):
        newValue = rescale(value, 0, 127, -45.0, 9)
#        scaledValue = pow(10, newValue * 0.05)
        self.volKnobMove(scaledValue)
        wx.CallAfter(self.volKnob.SetValue(newValue))
#        self.Layout()
        
    def getMidiScan(self, ctlnum, midichnl):
        self.assignMidiCtl(ctlnum)
        
    def assignMidiCtl(self, ctlnum, object=None):
        if object != None:
            self.pendingMidiObjects.append(object)
        for o in self.pendingMidiObjects:
            self.setMidiCtl(ctlnum, o)
            if o == "VolumeKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiVolKnob)
#                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.slider)
        self.pendingMidiObjects = []
        if self.midiscanning == True:
            self.midiscanning = False
            cGetMidiServer().ctlscan(None)
        
    def setMidiCtl(self, x, objectName):
        # This is because a parameter can have multiple midi ctls. They are stored in a list, inside the midiCtls dictionnary.
        if objectName in self.midiCtls:
            array = []
            if type(self.midiCtls[objectName]) is list:
                array = (self.midiCtls[objectName])
            else:
                array.append(self.midiCtls[objectName])
            array.append(x)
            self.midiCtls[objectName] = array
        else:
            self.midiCtls[objectName] = [x]
        wx.CallAfter(self.Refresh)
        
    def getMidiCtls(self):
        temp = {}
        for k in self.midiCtls:
            temp[k] = self.midiCtls.get(k, 0)
        return temp
        
#    def deleteOutput(self):
#        topParent = self._parent.GetParent()
#        
#        topParent.sizerOutputs.Hide(topParent._outputs[self._outputNo])
#        # To delete the associated (5,5) spacer
#        if self._outputNo != 0:
#            topParent.sizerOutputs.Remove((topParent._noOutputs*2)-((topParent._noOutputs-self._outputNo)*2))
#        else:
#            topParent.sizerOutputs.Remove(0)     
#        topParent.sizerOutputs.Remove(topParent._outputs[self._outputNo])
#                    
#        topParent._outputs[self._outputNo].Destroy
#        del topParent._outputs[self._outputNo]
#        
#        topParent._noOutputs -= 1
#        self._parent.Layout()
#        self._parent.FitInside()
#        topParent.Layout()
#        topParent.FitInside()

class MasterFXPanel(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=wx.RIGHT, name="Master FX Panel"):
        wx.Panel.__init__(self, parent, ID, pos, size, style, name)
        
        self._parent = parent
        self.midiscanning = False
        self.midiCtls = {}
        self.pendingMidiObjects = []
        
        self.fadePanel = wx.Panel(self)
        self.distPanel = wx.Panel(self)
        self.compPanel = wx.Panel(self)
        self.limiterPanel = wx.Panel(self)
        
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerFade = wx.GridBagSizer(0,0)
        self.sizerDist = wx.GridBagSizer(0,0)
        self.sizerComp = wx.GridBagSizer(0,0)
        self.sizerLimiter = wx.GridBagSizer(0,0)
        
        self.textFade = wx.StaticText(self.fadePanel, label="Crossfade Time (s)", size=wx.DefaultSize, style=wx.ALIGN_CENTER)
        font = self.textFade.GetFont()
        font.SetPointSize(15)
        self.textFade.SetFont(font)
        
        self.carFadeKnob = QLiveControlKnob(self.fadePanel, 0.05, 30.0, cGetCarrierFadeTime(), size=(90,60), knobRadius=40, label="Carrier", outFunction=self.carFadeKnobMove, name="carFadeKnob")
        self.modFadeKnob = QLiveControlKnob(self.fadePanel, 0.05, 30.0, cGetModulatorFadeTime(), size=(90,60), knobRadius=40, label="Modulator", outFunction=self.modFadeKnobMove, name="modFadeKnob")

        self.sizerFade.Add(self.textFade, pos=(0,0), span=(1,2), flag=wx.EXPAND|wx.ALIGN_CENTER, border=2)
        self.sizerFade.Add(self.carFadeKnob, pos=(1,0), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.sizerFade.Add(self.modFadeKnob, pos=(1,1), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.fadePanel.SetSizerAndFit(self.sizerFade)

        self.textDist = wx.StaticText(self.distPanel, label="Disto. (Outs)", size=wx.DefaultSize, style=wx.ALIGN_CENTER)
        font = self.textDist.GetFont()
        font.SetPointSize(15)
        self.textDist.SetFont(font)
        
        self.driveKnob = QLiveControlKnob(self.distPanel, 0.00, 0.95, cGetDrive(0), size=(90,60), knobRadius=40, label="Drive", outFunction=self.driveKnobMove, name="driveKnob")

        self.sizerDist.Add(self.textDist, pos=(0,0), flag=wx.EXPAND|wx.ALIGN_CENTER, border=2)
        self.sizerDist.Add(self.driveKnob, pos=(1,0))
        self.distPanel.SetSizerAndFit(self.sizerDist)

        self.textComp = wx.StaticText(self.compPanel, label="Compressor (Outs)", size=wx.DefaultSize, style=wx.ALIGN_CENTER)
        font = self.textComp.GetFont()
        font.SetPointSize(15)
        self.textComp.SetFont(font)
        
        self.threshKnob = QLiveControlKnob(self.compPanel, -50, 0, cGetThresh(0), size=(90,60), knobRadius=40, label="Thresh. (dB)", outFunction=self.threshKnobMove, name="ThreshKnob")
        self.ratioKnob = QLiveControlKnob(self.compPanel, 1, 20, cGetRatio(0), size=(90,60), knobRadius=40, label="Ratio (x:1)", outFunction=self.ratioKnobMove, name="RatioKnob")
        self.risetimeKnob = QLiveControlKnob(self.compPanel, 0.01, 3, cGetRisetime(0), size=(90,60), knobRadius=40, label="Rise Time (s)", outFunction=self.risetimeKnobMove, name="RisetimeKnob")
        self.falltimeKnob = QLiveControlKnob(self.compPanel, 0.01, 3, cGetFalltime(0), size=(90,60), knobRadius=40, label="Fall Time (s)", outFunction=self.falltimeKnobMove, name="FalltimeKnob")
#        self.lookaheadKnob = QLiveControlKnob(self.compPanel, 0, 25, cGetLookahead(0), size=(90,60), knobRadius=40, label="LookAhead (ms)", outFunction=self.lookaheadKnobMove, name="LookaheadKnob")
        self.kneeKnob = QLiveControlKnob(self.compPanel, 0.0, 1.0, cGetKnee(0), size=(90,60), knobRadius=40, label="Knee (h->s)", outFunction=self.kneeKnobMove, name="KneeKnob")

        self.sizerComp.Add(self.textComp, pos=(0,0), span=(1,5), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.sizerComp.Add(self.threshKnob, pos=(1,0))
        self.sizerComp.Add(self.ratioKnob, pos=(1,1))
        self.sizerComp.Add(self.kneeKnob, pos=(1,2))
        self.sizerComp.Add(self.risetimeKnob, pos=(1,3))
        self.sizerComp.Add(self.falltimeKnob, pos=(1,4))
#        self.sizerComp.Add(self.lookaheadKnob, pos=(1,5), flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.compPanel.SetSizerAndFit(self.sizerComp)
        
        self.textLimiter = wx.StaticText(self.limiterPanel, label="Limiter (Outs)", size=wx.DefaultSize, style=wx.ALIGN_CENTER)
        font = self.textLimiter.GetFont()
        font.SetPointSize(15)
        self.textLimiter.SetFont(font)
        
        self.maxKnob = QLiveControlKnob(self.limiterPanel, -60.0, 0.0, self.convertTodB(cGetMax(0)), size=(90,60), knobRadius=40, label="Max Level (dB)", outFunction=self.maxKnobMove, name="MaxKnob")
        
        self.sizerLimiter.Add(self.textLimiter, pos=(0,0),  flag=wx.EXPAND|wx.ALIGN_CENTER)
        self.sizerLimiter.Add(self.maxKnob, pos=(1,0))
        self.limiterPanel.SetSizerAndFit(self.sizerLimiter)

        self.sizer.Add((5,5), proportion=0)
        self.sizer.Add(self.fadePanel, flag=wx.ALIGN_CENTER|wx.EXPAND)
        self.sizer.Add((5,5), proportion=1)
        self.sizer.Add(self.distPanel, flag=wx.ALIGN_CENTER|wx.EXPAND)
        self.sizer.Add((5,5), proportion=1)
        self.sizer.Add(self.compPanel, flag=wx.ALIGN_CENTER|wx.EXPAND)
        self.sizer.Add((20,5), proportion=1)
        self.sizer.Add(self.limiterPanel, flag=wx.ALIGN_CENTER|wx.EXPAND)
        self.sizer.Add((5,5),proportion=0)
        self.SetSizerAndFit(self.sizer)
        
        # Event Binds
        self.carFadeKnob.Bind(wx.EVT_LEFT_DCLICK, self.carFadeKnobReset)
        self.modFadeKnob.Bind(wx.EVT_LEFT_DCLICK, self.modFadeKnobReset)
        self.driveKnob.Bind(wx.EVT_LEFT_DCLICK, self.driveKnobReset)
        self.threshKnob.Bind(wx.EVT_LEFT_DCLICK, self.threshKnobReset)
        self.ratioKnob.Bind(wx.EVT_LEFT_DCLICK, self.ratioKnobReset)
        self.risetimeKnob.Bind(wx.EVT_LEFT_DCLICK, self.risetimeKnobReset)
        self.falltimeKnob.Bind(wx.EVT_LEFT_DCLICK, self.falltimeKnobReset)
        self.kneeKnob.Bind(wx.EVT_LEFT_DCLICK, self.kneeKnobReset)
        self.maxKnob.Bind(wx.EVT_LEFT_DCLICK, self.maxKnobReset)
        
        self.carFadeKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.modFadeKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.driveKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.threshKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.ratioKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.risetimeKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.falltimeKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.kneeKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)
        self.maxKnob.Bind(wx.EVT_RIGHT_DOWN, self.onMouseRightDown)

    def convertTodB(self, floatValue):
        try:
            return math.log10(floatValue) * 10
        except ValueError:
            return -60.0
            
    def convertToMul(self, floatValue):
        return pow(10, (floatValue/10))

    def carFadeKnobMove(self, value):
        cSetCarrierFadeTime(value)
        self.Refresh()
    
    def modFadeKnobMove(self, value):
        cSetModulatorFadeTime(value)
        self.Refresh()
        
    def driveKnobMove(self, value):
        cSetDrive(0, value)
        self.Refresh()

    def threshKnobMove(self, value):
        cSetThresh(0, value)
        self.Refresh()
    
    def ratioKnobMove(self, value):
        cSetRatio(0, value)
        self.Refresh()
    
    def risetimeKnobMove(self, value):
        cSetRisetime(0, value)
        self.Refresh()
    
    def falltimeKnobMove(self, value):
        cSetFalltime(0, value)
        self.Refresh()
    
    def lookaheadKnobMove(self, value):
        cSetLookahead(0, value)
        self.Refresh()
    
    def kneeKnobMove(self, value):
        cSetKnee(0, value)
        self.Refresh()
    
    def maxKnobMove(self, value):
        cSetMax(0, self.convertToMul(value))
        self.Refresh()
    
    def carFadeKnobReset(self, e):
        value = 0.05
        e.GetEventObject().SetValue(value)
        self.Refresh()
        
    def modFadeKnobReset(self, e):
        value = 0.05
        e.GetEventObject().SetValue(value)
        self.Refresh()

    def driveKnobReset(self, e):
        value = 0.0
        e.GetEventObject().SetValue(value)
        self.driveKnobMove(value)
        self.Refresh()

    def threshKnobReset(self, e):
        value = -20.0
        e.GetEventObject().SetValue(value)
        self.threshKnobMove(value)
        self.Refresh()
    
    def ratioKnobReset(self, e):
        value = 2.0
        e.GetEventObject().SetValue(value)
        self.ratioKnobMove(value)
        self.Refresh()
    
    def risetimeKnobReset(self, e):
        value = 0.01
        e.GetEventObject().SetValue(value)
        self.risetimeKnobMove(value)
        self.Refresh()
    
    def falltimeKnobReset(self, e):
        value = 0.1
        e.GetEventObject().SetValue(value)
        self.falltimeKnobMove(value)
        self.Refresh()
    
    def lookaheadKnobReset(self, e):
        value = 5.0
        e.GetEventObject().SetValue(value)
        self.lookaheadKnobMove(value)
        self.Refresh()
    
    def kneeKnobReset(self, e):
        value = 0
        e.GetEventObject().SetValue(value)
        self.kneeKnobMove(value)
        self.Refresh()
    
    def maxKnobReset(self, e):
        value = 1.0
        e.GetEventObject().SetValue(value)
        self.maxKnobMove(value)
        self.Refresh()
        
    def onMouseRightDown(self, evt):
        if evt.ShiftDown():
            if evt.AltDown():
                o = evt.GetEventObject().GetName()
                try:
                    ctlnum = self.midiCtls[o]
                    # We delete all ctls associated with the parameter
                    for c in ctlnum:
                        if o == "carFadeKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiCarFadeKnob)
                        elif o == "modFadeKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiModFadeKnob)
                        elif o == "driveKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiDriveKnob)
                        elif o == "ThreshKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiThreshKnob)
                        elif o == "RatioKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiRatioKnob)
                        elif o == "RisetimeKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiRisetimeKnob)
                        elif o == "FalltimeKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiFalltimeKnob)
                        elif o == "KneeKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiKneeKnob)
                        elif o == "MaxKnob":
                            cGetMidiServer().unbind("ctls", c, self.midiMaxKnob)
                    self.setMidiCtl(None, o)
                    del self.midiCtls[o]
                    sRevertMidiBackgroundColour(evt.GetEventObject())
                    return
                except KeyError:
                    pass
            if not self.midiscanning:
                self.midiscanning = True
                self.pendingMidiObjects.append(evt.GetEventObject().GetName())
                cGetMidiServer().ctlscan(self.getMidiScan)

    def getMidiScan(self, ctlnum, midichnl):
        self.assignMidiCtl(ctlnum)
        
    def assignMidiCtl(self, ctlnum, object=None):
        if object != None:
            self.pendingMidiObjects.append(object)
        for o in self.pendingMidiObjects:
            self.setMidiCtl(ctlnum, o)
            if o == "carFadeKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiCarFadeKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.carFadeKnob)
            elif o == "modFadeKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiModFadeKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.modFadeKnob)
            elif o == "driveKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiDriveKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.driveKnob)
            elif o == "ThreshKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiThreshKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.threshKnob)
            elif o == "RatioKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiRatioKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.ratioKnob)
            elif o == "RisetimeKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiRisetimeKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.risetimeKnob)
            elif o == "FalltimeKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiFalltimeKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.falltimeKnob)
            elif o == "KneeKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiKneeKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.kneeKnob)
            elif o == "MaxKnob":
                cGetMidiServer().bind("ctls", ctlnum, self.midiMaxKnob)
                sSetMidiBackgroundColour(MIDILEARN_COLOUR, self.maxKnob)
        self.pendingMidiObjects = []
        if self.midiscanning == True:
            self.midiscanning = False
            cGetMidiServer().ctlscan(None)
        
    def setMidiCtl(self, x, objectName):
        # This is because a parameter can have multiple midi ctls. They are stored in a list, inside the midiCtls dictionnary.
        if objectName in self.midiCtls:
            array = []
            if type(self.midiCtls[objectName]) is list:
                array = (self.midiCtls[objectName])
            else:
                array.append(self.midiCtls[objectName])
            array.append(x)
            self.midiCtls[objectName] = array
        else:
            self.midiCtls[objectName] = [x]
        wx.CallAfter(self.Refresh)
        
    def getMidiCtls(self):
        temp = {}
        for k in self.midiCtls:
            temp[k] = self.midiCtls.get(k, 0)
        return temp
        
    def midiCarFadeKnob(self, value):
        newValue = rescale(value, 0, 127, 0.05, 30.0)
        cSetCarrierFadeTime(newValue)
        wx.CallAfter(self.carFadeKnob.SetValue(newValue))
#        self.Layout()
        
    def midiModFadeKnob(self, value):
        newValue = rescale(value, 0, 127, 0.05, 30.0)
        cSetModulatorFadeTime(newValue)
        wx.CallAfter(self.modFadeKnob.SetValue(newValue))
#        self.Layout()
    
    def midiDriveKnob(self, value):
        newValue = rescale(value, 0, 127, 0.0, 0.95)
        cSetDrive(0, newValue)
        wx.CallAfter(self.driveKnob.SetValue(newValue))
#        self.Layout()

    def midiThreshKnob(self, value):
        newValue = rescale(value, 0, 127, -50.0, 0.0)
        cSetThresh(0, newValue)
        wx.CallAfter(self.threshKnob.SetValue(newValue))
#        self.Layout()
        
    def midiRatioKnob(self, value):
        newValue = rescale(value, 0, 127, 1.0, 20.0)
        cSetRatio(0, newValue)
        wx.CallAfter(self.ratioKnob.SetValue(newValue))
#        self.Layout()

    def midiRisetimeKnob(self, value):
        newValue = rescale(value, 0, 127, 0.01, 3.0)
        cSetRisetime(0, newValue)
        wx.CallAfter(self.risetimeKnob.SetValue(newValue))
#        self.Layout()
        
    def midiFalltimeKnob(self, value):
        newValue = rescale(value, 0, 127, 0.01, 3.0)
        cSetFalltime(0, newValue)
        wx.CallAfter(self.falltimeKnob.SetValue(newValue))
#        self.Layout()

    def midiKneeKnob(self, value):
        newValue = rescale(value, 0, 127, 0.0, 1.0)
        cSetKnee(0, newValue)
        wx.CallAfter(self.kneeKnob.SetValue(newValue))
#        self.Layout()
        
    def midiMaxKnob(self, value):
        newValue = rescale(value, 0, 127, -60.0, 0.0)
        cSetMax(0, self.convertToMul(newValue))
        wx.CallAfter(self.maxKnob.SetValue(newValue))
#        self.Layout()

