#!/usr/bin/env python
#coding: utf8

from pyo import *
from RingMod import RingMod

s = Server().boot()

# Inputs
src1 = SfPlayer("transparent.aif", loop=True, mul=.3)
src2 = SuperSaw(800, 0.5)
src3 = Sine(400)
src4 = SfPlayer("transparent.aif", loop=True, mul=.3)


ring = RingMod(src1, src2, mul=[1,1]).out()
# Split the resulting signal in 4 voices, to be treated separately
# and outputted to different channels
outs = Switch(ring, 4)


s.gui(locals())
