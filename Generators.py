#!/usr/bin/env python
#coding: utf8

from pyo import *

class InputSine(PyoObject):
    """
    Input Sine
    
    Generates a pure sine signal and its mean amplitude.

    :Parent: :py:class::'PyoObject'
    
    :Args:
        
        freq : float or PyoObject, optional
            Frequency of the sine. Defaults to 1000.
            
    """
    def __init__(self, freq=1000, mul=1, add=0):
        PyoObject.__init__(self, mul, add)
        self._freq = freq
        self._sine = Sine(freq)
        self._input_fader = InputFader(self._sine)
        input_fader,freq,mul,add,lmax = convertArgsToLists(self._input_fader,self._freq,mul,add)
        self._sig = Sig(input_fader)
        self._audio = Sig(self._sig, mul=mul, add=add)
        self._inputPeak = PeakAmp(Port(self._audio))
        self._base_objs = self._audio.getBaseObjects()
        
    def setFreq(self, x):
        """
        Replace the 'freq' attribute.
        
        :Args:
            
            x : float or PyoObject
            New `freq` attribute.
                
        """
        self._freq = x
        self._sine.setFreq(x)
        
    def play(self, dur=0, delay=0):
        self._audio.play(dur, delay)
        return PyoObject.play(self, dur, delay)
        
    def stop(self):
        self._audio.stop()
        return PyoObject.stop(self)
        
    def out(self, chnl=0, inc=1, dur=0, delay=0):
        self._audio.play(dur, delay)
        return PyoObject.out(self, chnl, inc, dur, delay)
        
    def ctrl(self, map_list=None, title=None, wxnoserver=False):
        self._map_list = [SLMapFreq(self._freq),
                          SLMapMul(self._mul)]
        PyoObject.ctrl(self, map_list, title, wxnoserver)
        
    def save(self):
        dict = {'freq': self._freq, 'mul': self._mul}
        return dict
        
    @property
    def freq(self):
        """float or PyoObject.  Frequency of the sine."""
        return self._freq
    @freq.setter
    def freq(self, x):
        self.setFreq(x)
        
    @property
    def inputPeak(self):
        """PyoObject. Peak follower of the sine signal. For use with a VuMeter."""
        return self._inputPeak
        
class InputNoise(PyoObject):
    """
    Input Noise
    
    Generates white noise and outputs a
    band-pass filtered signal and its mean amplitude.

    :Parent: :py:class::'PyoObject'
    
    :Args:
        
        freq : float or PyoObject, optional
            Frequency of the band-pass filter. Defaults to 1000.
        q : float or PyoObject, optional
            Q of the filter, defined as freq/bandwidth.
            Should be between 1 and 500. Defaults to 1.
        bypass : boolean, optional
            Specifies if the band-pass filter is bypassed or not.
            Defaults to False.
            
    """
    def __init__(self, freq=1000, q=1, bypass=True, mul=1, add=0):
        PyoObject.__init__(self, mul, add)
        self._freq = freq
        self._q = q
        self._bypass = bypass
        self._noise = Noise()
        self._input_fader = InputFader(self._noise)
        input_fader,freq,q,bypass,mul,add,lmax = convertArgsToLists(self._input_fader,self._freq,self._q,self._bypass,mul,add)
        self._bpf = ButBP(input_fader, freq, q)
        self._sig = Sig(input_fader)
        if self._bypass == False:
            self._audio = Sig(self._bpf, mul=mul, add=add)
        else:
            self._audio = Sig(self._sig, mul=mul, add=add)
        self._inputPeak = PeakAmp(Port(self._audio))
        self._base_objs = self._audio.getBaseObjects()
        
    def setFreq(self, x):
        """
        Replace the 'freq' attribute.
        
        :Args:
            
            x : float or PyoObject
            New `freq` attribute.
                
        """
        self._freq = x
        self._bpf.setFreq(x)
        
    def setQ(self, x):
        """
        Replace the `q` attribute. Should be between 1 and 500.
        
        :Args:
            
            x : float or PyoObject
            New `q` attribute
                
        """
        self._q = x
        self._bpf.setQ(x)
        
    def setBypass(self, x):
        """
        Replace the `bypass` attribute.
        
        :Args:
            
            x : boolean
            New `bypass` attribute
                
        """
        self._bypass = x
        if x == True:
            self._audio.setValue(self._sig)
        elif x == False:
            self._audio.setValue(self._bpf)
        
    def play(self, dur=0, delay=0):
        self._audio.play(dur, delay)
        return PyoObject.play(self, dur, delay)
        
    def stop(self):
        self._audio.stop()
        return PyoObject.stop(self)
        
    def out(self, chnl=0, inc=1, dur=0, delay=0):
        self._audio.play(dur, delay)
        return PyoObject.out(self, chnl, inc, dur, delay)
        
    def ctrl(self, map_list=None, title=None, wxnoserver=False):
        self._map_list = [SLMapFreq(self._freq),
                          SLMapQ(self._q),
                          SLMap(0, 1, "lin", "Bypass", self._bypass, "int", dataOnly = True),
                          SLMapMul(self._mul)]
        PyoObject.ctrl(self, map_list, title, wxnoserver)
        
    def save(self):
        dict = {'freq': self._freq, 'q': self._q, 'bypass': self._bypass, 'mul': self._mul}
        return dict
        
    @property
    def freq(self):
        """float or PyoObject.  Frequency of the band-pass filter."""
        return self._freq
    @freq.setter
    def freq(self, x):
        self.setFreq(x)
        
    @property
    def q(self):
        """float or PyoObject. Q of the filter, defined as freq/bandwidth."""
        return self._q
    @q.setter
    def q(self, x):
        self.setQ(x)
        
    @property
    def bypass(self):
        """boolean. Specifies if the band-pass filter is bypassed or not."""
        return self._bypass
    @bypass.setter
    def bypass(self, x):
        self.setBypass(x)
        
    @property
    def inputPeak(self):
        """PyoObject. Peak follower of the input signal. For use with a VuMeter."""
        return self._inputPeak