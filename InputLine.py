#!/usr/bin/env python
#coding: utf8

from pyo import *

class InputLine(PyoObject):
    """
    Input Line
    
    Takes an input from a specified channel and outputs a
    band-pass filtered signal and its mean amplitude.

    :Parent: :py:class::'PyoObject'
    
    :Args:
        
        chnl : int, optional
            Channel to read from. Defaults to 0.
        freq : float or PyoObject, optional
            Frequency of the band-pass filter. Defaults to 1000.
        q : float or PyoObject, optional
            Q of the filter, defined as freq/bandwidth.
            Should be between 1 and 500. Defaults to 1.
        bypass : boolean, optional
            Specifies if the band-pass filter is bypassed or not.
            Defaults to False.
        slope : float or PyoObject, optional
            Attack and release slope for the Follower in seconds. Default to 0.01.
            
    """
    def __init__(self, chnl=0, freq=1000, q=1, bypass=True, shift=0, slope=0.01, mul=1, add=0):
        PyoObject.__init__(self, mul, add)
        self._chnl = chnl
        self._freq = freq
        self._q = q
        self._bypass = bypass
        self._shift = shift
        self._slope = slope
        self._input = Input(chnl)
        self._input_fader = InputFader(self._input)
        input_fader,freq,q,bypass,shift,slope,mul,add,lmax = convertArgsToLists(self._input_fader,self._freq,self._q,self._bypass,self._shift,self._slope,mul,add)
        self._bpf = ButBP(input_fader, freq, q)
        self._sig = Sig(input_fader)
        if self._bypass == False:
            self._post = Sig(self._bpf, mul=mul, add=add)
        else:
            self._post = Sig(self._sig, mul=mul, add=add)
        self._shifter = FreqShift(self._post, shift) 
        self._audio = self._shifter
        self._inputPeak = PeakAmp(Port(self._audio))
        self._follower = Follower2(self._audio, slope, slope, mul=5) 
        self._base_objs = self._audio.getBaseObjects()

    def setChnl(self, x, fadetime=0.05):
        """
        Replace the 'chnl' attribute.
        
        :Args:
            
            x : int
                New chnl to read from.
            fadetime : float, optional
                Crossfade time between old and new input. Defaults to 0.05.
                
        """
        self._chnl = x
        self._input = Input(x) 
        self._input_fader.setInput(self._input, fadetime)
        
    def setFreq(self, x):
        """
        Replace the 'freq' attribute.
        
        :Args:
            
            x : float or PyoObject
            New `freq` attribute.
                
        """
        self._freq = x
        self._bpf.setFreq(x)
        
    def setQ(self, x):
        """
        Replace the `q` attribute. Should be between 1 and 500.
        
        :Args:
            
            x : float or PyoObject
            New `q` attribute
                
        """
        self._q = x
        self._bpf.setQ(x)
        
    def setBypass(self, x):
        """
        Replace the `bypass` attribute.
        
        :Args:
            
            x : boolean
            New `bypass` attribute
                
        """
        self._bypass = x
        if x == True:
            self._post.setValue(self._sig)
        elif x == False:
            self._post.setValue(self._bpf)
        
    def setShift(self, x):
        """
        Replace the 'shift' attribute.
        
        :Args:
            
            x : float or PyoObject
            New `shift` attribute.
                
        """
        self._shift = x
        self._shifter.setShift(x)

    def setSlope(self, x):
        """
        Replace the 'slope' attribute.
        
        :Args:
            
            x : float or PyoObject
            New `slope` attribute.
                
        """
        self._slope = x
        self._follower.setRisetime(x)
        self._follower.setFalltime(x)
        
    def play(self, dur=0, delay=0):
        self._audio.play(dur, delay)
        return PyoObject.play(self, dur, delay)
        
    def stop(self):
        self._audio.stop()
        return PyoObject.stop(self)
        
    def out(self, chnl=0, inc=1, dur=0, delay=0):
        self._audio.play(dur, delay)
        return PyoObject.out(self, chnl, inc, dur, delay)
        
    def ctrl(self, map_list=None, title=None, wxnoserver=False):
        self._map_list = [SLMapFreq(self._freq),
                          SLMapQ(self._q),
                          SLMap(0, 1, "lin", "Bypass", self._bypass, "int", dataOnly = True),
                          SLMapFreq(self._shift),
                          SLMapFreq(self._slope),
                          SLMapMul(self._mul)]
        PyoObject.ctrl(self, map_list, title, wxnoserver)
        
    def save(self):
        dict = {'chnl': self._chnl, 'freq': self._freq, 'q': self._q, 'bypass': self._bypass, 'shift': self._shift, 'slope': self._slope, 'mul': self._mul}
        return dict
        
    @property # getter
    def chnl(self):
        """int. Channel to read from."""
        return self._chnl
    @chnl.setter # setter
    def chnl(self, x):
        self.setChnl(x)
        
    @property
    def freq(self):
        """float or PyoObject.  Frequency of the band-pass filter."""
        return self._freq
    @freq.setter
    def freq(self, x):
        self.setFreq(x)
        
    @property
    def q(self):
        """float or PyoObject. Q of the filter, defined as freq/bandwidth."""
        return self._q
    @q.setter
    def q(self, x):
        self.setQ(x)
        
    @property
    def bypass(self):
        """boolean. Specifies if the band-pass filter is bypassed or not."""
        return self._bypass
    @bypass.setter
    def bypass(self, x):
        self.setBypass(x)

    @property
    def shift(self):
        """float or PyoObject. Amount of shifting in Hertz."""
        return self._shift
    @shift.setter
    def shift(self, x):
        self.setShift(x)

    @property
    def slope(self):
        """float or PyoObject. Rise and fall time of the Follower."""
        return self._slope
    @slope.setter
    def slope(self, x):
        self.setSlope(x)
        
    @property
    def follower(self):
        """PyoObject. Enveloppe follower of the input signal."""
        return self._follower
        
    @property
    def inputPeak(self):
        """PyoObject. Peak follower of the input signal. For use with a VuMeter."""
        return self._inputPeak