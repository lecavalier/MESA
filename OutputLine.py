#!/usr/bin/env python
#coding: utf8

import math
from pyo import *

#FEATURE POSSIBLE: Ajouter filtres à cet objet
class OutputLine(PyoObject):
    """
    Output Line
    
    Takes an input signal and outputs an apmlitude-modulated signal.

    :Parent: :py:class::'PyoObject'
    
    :Args:
        
        input : PyoObject
            Input signal to process. Defaults to 0.
        env : float or PyoObject, optional
            Amplitude-modulating signal to be applied to the input.
            Defaults to 1.
            
    """
    def __init__(self, input, drive=0, thresh=-20, ratio=2, risetime=0.01, falltime=0.1, lookahead=5.0, knee=0, max=1, env=1, mul=1, add=0):
        PyoObject.__init__(self, mul, add)
        self._input = input
        self._drive = drive
        self._thresh = thresh
        self._ratio = ratio
        self._risetime = risetime
        self._falltime = falltime
        self._lookahead = lookahead
        self._knee = knee
        self._max = max
        self._env = env
        self._input_fader = InputFader(self._input)
        self._env_fader = InputFader(self._env)
        input_fader,drive,thresh,ratio,risetime,falltime,lookahead,knee,max,env_fader,mul,add,lmax = convertArgsToLists(self._input_fader,self._drive,self._thresh,self._ratio,self._risetime,self._falltime,self._lookahead,self._knee,self._max,self._env_fader,self._mul,add)
        self._mod = Sig(input_fader, mul=env_fader)
        self._disto = Disto(self._mod, drive)
        self._comp = Compress(self._disto, thresh, ratio, risetime, falltime, lookahead, knee, mul=[math.log10(x)+1.00 for x in ratio])
        self._sig = Sig(self._comp, mul=mul, add=add)
        self._clip = Clip(self._sig, max=max)
        self._audio = Sig(self._clip)
        self._outputPeak = PeakAmp(Port(self._audio))
        self._base_objs = self._audio.getBaseObjects()

    def setInput(self, x, fadetime=0.05):
        """
        Replace the 'input' attribute.
        
        :Args:
            
            x : PyoObject
                New signal to process.
            fadetime : float, optional
                Crossfade time between old and new input. Defaults to 0.05.
                
        """
        self._input = x
        self._input_fader.setInput(x, fadetime)
       
    def setEnv(self, x, fadetime=0.05):
        """
        Replace the 'env' attribute.
        
        :Args:
            
            x : PyoObject
                New enveloppe signal.
            fadetime : float, optional
                Crossfade time between old and new input. Defaults to 0.05.
                
        """
        self._env = x
        self._env_fader.setInput(x, fadetime)
       
    def setDrive(self, x):
        """
        Replace the 'drive' attribute.
        
        :Args:
            
            x : PyoObject
                New drive value.
                
        """
        self._drive = x
        self._disto.setDrive(x)
 
    def setThresh(self, x):
        """
        Replace the 'thresh' attribute.
        
        :Args:
            
            x : PyoObject
                New thresh value.
                
        """
        self._thresh = x
        self._comp.setThresh(x)
        
    def setRatio(self, x):
        """
        Replace the 'ratio' attribute.
        
        :Args:
            
            x : PyoObject
                New ratio value.
                
        """
        self._ratio = x
        self._comp.setRatio(x)
        self._comp.setMul(math.log10(x)+1.00)
        
    def setRisetime(self, x):
        """
        Replace the 'risetime' attribute.
        
        :Args:
            
            x : PyoObject
                New risetime value.
                
        """
        self._risetime = x
        self._comp.setRiseTime(x)

    def setFalltime(self, x):
        """
        Replace the 'falltime' attribute.
        
        :Args:
            
            x : PyoObject
                New falltime value.
                
        """
        self._falltime = x
        self._comp.setFallTime(x)
        
    def setLookahead(self, x):
        """
        Replace the 'lookahead' attribute.
        
        :Args:
            
            x : PyoObject
                New lookahead value.
                
        """
        self._lookahead = x
        self._comp.setLookAhead(x)
        
    def setKnee(self, x):
        """
        Replace the 'knee' attribute.
        
        :Args:
            
            x : PyoObject
                New knee value.
                
        """
        self._knee = x
        self._comp.setKnee(x)

    def setMax(self, x):
        """
        Replace the 'max' attribute.
        
        :Args:
            
            x : PyoObject
                New max value.
                
        """
        self._max = x
        self._clip.setMax(x)

    def mute(self, state):
        """
        Mutes the output.
        
        :Args:
            
            state : boolean
                0 = Unmoute the output
                1 = Mute the outpute
                
        """
        if state == 1:
            self._input_fader.setInput(Sig(0))
        else:
            self._input_fader.setInput(self._input)
        
    def play(self, dur=0, delay=0):
        self._audio.play(dur, delay)
        return PyoObject.play(self, dur, delay)
        
    def stop(self):
        self._audio.stop()
        return PyoObject.stop(self)
        
    def out(self, chnl=0, inc=1, dur=0, delay=0):
        self._audio.play(dur, delay)
        return PyoObject.out(self, chnl, inc, dur, delay)
        
    def ctrl(self, map_list=None, title=None, wxnoserver=False):
        self._map_list = [SLMapMul(self._mul)]
        PyoObject.ctrl(self, map_list, title, wxnoserver)
        
    def save(self):
        dict = {'drive': self._drive, 'thresh': self._thresh, 'ratio': self._ratio, 'risetime': self._risetime, 'falltime': self._falltime, 'lookahead': self._lookahead, 'knee': self._knee, 'max': self._max, 'mul': self._mul}
        return dict
        
    @property # getter
    def input(self):
        """PyoObject. Input signal to process."""
        return self._input
    @input.setter # setter
    def input(self, x):
        self.setInput(x)
        
    @property
    def drive(self):
        """PyoObject. Distortion amount."""
        return self._drive
    @drive.setter
    def drive(self, x):
        self.setDrive(x)
        
    @property
    def thresh(self):
        """PyoObject. Compressor threshold level."""
        return self._thresh
    @thresh.setter
    def thresh(self, x):
        self.setThresh(x)
        
    @property
    def ratio(self):
        """PyoObject. Compressor ratio."""
        return self._ratio
    @ratio.setter
    def ratio(self, x):
        self.setRatio(x)
        
    @property
    def risetime(self):
        """PyoObject. Compressor risetime in seconds."""
        return self._risetime
    @risetime.setter
    def risetime(self, x):
        self.setRisetime(x)

    @property
    def falltime(self):
        """PyoObject. Compressor falltime in seconds."""
        return self._falltime
    @falltime.setter
    def falltime(self, x):
        self.setFalltime(x)

    @property
    def lookahead(self):
        """PyoObject. Compressor lookahead in milliseconds."""
        return self._lookahead
    @lookahead.setter
    def lookahead(self, x):
        self.setLookahead(x)
        
    @property
    def knee(self):
        """PyoObject. Compressor knee. (0 to 1)"""
        return self._knee
    @knee.setter
    def knee(self, x):
        self.setKnee(x)

    @property
    def max(self):
        """PyoObject. Limiter clip level."""
        return self._max
    @max.setter
    def max(self, x):
        self.setMax(x)

    @property
    def env(self):
        """PyoObject. Amp enveloppe signal."""
        return self._env
    @env.setter
    def env(self, x):
        self.setEnv(x)

    @property
    def outputPeak(self):
        """PyoObject. Peak follower of the output signal. For use with a VuMeter."""
        return self._outputPeak