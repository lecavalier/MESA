#!/usr/bin/env python
#coding: utf8
from pyo import *
from RingModInstrument import *

# Getters
# Used to set the input VuMeter
def cGetInputPeak(inputNo):
    return getInputPeak(inputNo)
    
def cGetSinePeak():
    return getSinePeak()
    
def cGetNoisePeak():
    return getNoisePeak()
    
# Used to set the output VuMeter
def cGetOutputPeak(outputNo):
    return getOutputPeak(outputNo)
    
def cGetFilterFreq(inputNo):
    return getFilterFreq(inputNo)
    
def cGetFilterQ(inputNo):
    return getFilterQ(inputNo)

def cGetSlope(inputNo):
    return getSlope(inputNo)
    
def cGetShift(inputNo):
    return getShift(inputNo)
    
def cGetInputGain(inputNo):
    return getInputGain(inputNo)
 
# Si on veut, dans le futur, ajouter plusieurs sinus, on a qu'à créer unarray de sinus et aller chercher le bon sinus avec un index (comme les ins)   
def cGetSineFreq():
    return getSineFreq()
    
def cGetSineGain():
    return getSineGain()
    
def cGetNoiseFilterFreq():
    return getNoiseFilterFreq()
    
def cGetNoiseFilterQ():
    return getNoiseFilterQ()
    
def cGetNoiseGain():
    return getNoiseGain()
    
# Returns the 'mul' value of one output
def cGetOutputMul(outputNo):
    return getOutputMul(outputNo)
    
def cIsCarrier(inputNo):
    return isCarrier(inputNo)
    
def cIsModulator(inputNo):
    return isModulator(inputNo)
    
def cIsFilterBypassed(inputNo):
    return isFilterBypassed(inputNo)
    
def cIsNoiseFilterBypassed():
    return isNoiseFilterBypassed()
    
def cGetDrive(outputNo):
    return getDrive(outputNo)
    
def cGetThresh(outputNo):
    return getThresh(outputNo)
    
def cGetRatio(outputNo):
    return getRatio(outputNo)
    
def cGetRisetime(outputNo):
    return getRisetime(outputNo)
    
def cGetFalltime(outputNo):
    return getFalltime(outputNo)
    
def cGetLookahead(outputNo):
    return getLookahead(outputNo)
    
def cGetKnee(outputNo):
    return getKnee(outputNo)
    
def cGetMax(outputNo):
    return getMax(outputNo)
    
def cGetCarrierFadeTime():
    return getCarrierFadeTime()
  
def cGetModulatorFadeTime():
    return getModulatorFadeTime()
      
# Setters
def cSetFilterFreq(inputNo, value):
    setFilterFreq(inputNo, value)
    
def cSetFilterQ(inputNo, value):
    setFilterQ(inputNo, value)

def cSetSlope(inputNo, value):
    setSlope(inputNo, value)
    
def cSetShift(inputNo, value):
    setShift(inputNo, value)
 
def cSetInputGain(inputNo, value):
    setInputGain(inputNo, value)
       
def cToggleBypass(inputNo, state):
    toggleFilterBypass(inputNo, state)
    
def cToggleCarrier(inputNo, state):
    toggleCarrier(inputNo, state)

def cToggleModulator(inputNo, state):
    toggleModulator(inputNo, state)
    
def cSetOutputMul(outputNo, value):
    setOutputMul(outputNo, value)
    
def cSetSineFreq(value):
    setSineFreq(value)

def cSetSineGain(value):
    setSineGain(value)
    
def cSetNoiseFilterFreq(value):
    setNoiseFilterFreq(value)
    
def cSetNoiseGain(value):
    setNoiseGain(value)
    
def cSetNoiseFilterQ(value):
    setNoiseFilterQ(value)
    
def cToggleBypassNoise(state):
    toggleBypassNoise(state)

def cToggleModulatorSine(state):
    toggleModulatorSine(state)
    
def cToggleCarrierSine(state):
    toggleCarrierSine(state)
    
def cToggleModulatorNoise(state):
    toggleModulatorNoise(state)
    
def cToggleCarrierNoise(state):
    toggleCarrierNoise(state)
    
def cToggleMuteAll(state):
    toggleMuteAll(state)
    
def cSetDrive(outputNo, x):
    setDrive(outputNo, x)
    
def cSetThresh(outputNo, x):
    setThresh(outputNo, x)
    
def cSetRatio(outputNo, x):
    setRatio(outputNo, x)
    
def cSetRisetime(outputNo, x):
    setRisetime(outputNo, x)
    
def cSetFalltime(outputNo, x):
    setFalltime(outputNo, x)
    
def cSetLookahead(outputNo, x):
    setLookahead(outputNo, x)
    
def cSetKnee(outputNo, x):
    setKnee(outputNo, x)
    
def cSetMax(outputNo, x):
    setMax(outputNo, x)
    
def cSetCarrierFadeTime(value):
    setCarrierFadeTime(value)
    
def cSetModulatorFadeTime(value):
    setModulatorFadeTime(value)
    
# Functions related to the Save and Open function
def cGetSessionName():
    return getSessionName()
    
def cGetSaveDir():
    return getSaveDir()
    
def cSaveSession(fn, dir, ctls):
    saveSession(fn, dir, ctls)
    
def cOpenSession(path, frame):
    openSessionFile(path, frame)
    
def cGetMidiServer():
    return getMidiServer()
    